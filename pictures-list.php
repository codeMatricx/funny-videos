<?php 
 include "database.php";
$pcid = $_GET['picture_cat_id'];
?>
<!--header-->
<?php include"include/header.php"; ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			<div class="main-grids">
				<div class="recommended">
					<div class="recommended-grids english-grid">
						<div class="recommended-info">
							<?php 
		                    $sql = "SELECT * FROM category WHERE id=$pcid ";
		                    $result = $conn->query($sql);
		                    if ($result->num_rows>0)
		                    {
		                    while($category = $result->fetch_assoc())
		                    {
		                    ?>
							<div class="heading">
								<h3>Top <?php echo $category['category_name'];?></h3>
							</div>
							<?php 
	                     	} 
	                    	}
	                     	?>
							<div class="clearfix"> </div>
						</div>
						<?php 
	                    $sql = "SELECT * FROM pictures WHERE categories=$pcid ";
	                    $result = $conn->query($sql);
	                    if ($result->num_rows>0)
	                    {
	                    while($pic_cat = $result->fetch_assoc())
	                    {
	                    ?>
						<div class="col-md-3 resent-grid recommended-grid sports-recommended-grid">
							<div class="resent-grid-img recommended-grid-img">
								<a href="pictures.php?pics_id=<?php echo $pic_cat['id'];?>&pic_cat_id=<?php echo $pic_cat['categories'];?>"><img src="images/<?php echo $pic_cat['image'];?>" alt="" height="250px" /></a>
							</div>
							<div class="resent-grid-info recommended-grid-info">
								<h5><a href="pictures.php?pics_id=<?php echo $pic_cat['id'];?>&pic_cat_id=<?php echo $pic_cat['categories'];?>" class="title"><?php echo $pic_cat['item_name'];?></a></h5>
								<p class="author"><a href="#" class="author">Admin</a></p>
								<p class="views"><?php echo $pic_cat['view_count'];?> views</p>
							</div>
						</div>
						<?php 
                     	} 
                    	}
                     	?>
						<div class="clearfix"> </div>
					</div>
				</div>
			</div>
<!-- footer -->
<?php include "include/footer.php"; ?>			