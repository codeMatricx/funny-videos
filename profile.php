 
<!--header-->
<?php include"include/header.php"; ?>
<?php 

if (isset($_POST['deleteVideo']))
{
    $id = ($_POST['id']);
    
    $sql = "DELETE FROM videos WHERE id= $id";
    if ($conn->query($sql) === TRUE)
    {
        //unlink($image_dir.$deleteimage);
        $responseMessage =  "User Remove successfully";
    }
    else
    {
        $responseMessage =  "Connection failed: " . $conn->error;
    }
}
if (isset($_POST['deleteGif']))
{
    $id = ($_POST['id']);
    
    $sql = "DELETE FROM gifs WHERE id= $id";
    if ($conn->query($sql) === TRUE)
    {
        //unlink($image_dir.$deleteimage);
        $responseMessage =  "User Remove successfully";
    }
    else
    {
        $responseMessage =  "Connection failed: " . $conn->error;
    }
}
if (isset($_POST['deletePic']))
{
    $id = ($_POST['id']);
    
    $sql = "DELETE FROM pictures WHERE id= $id";
    if ($conn->query($sql) === TRUE)
    {
        //unlink($image_dir.$deleteimage);
        $responseMessage =  "User Remove successfully";
    }
    else
    {
        $responseMessage =  "Connection failed: " . $conn->error;
    }
}

?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		  <ul class="nav nav-tabs profile_nav" style="margin-top:-19px">
			<li class="active"><a data-toggle="tab" href="#home">Profile Detail</a></li>
			<li><a data-toggle="tab" href="#menu1">Your Videos</a></li>
			<li><a data-toggle="tab" href="#menu2">Your Gifs</a></li>
			<li><a data-toggle="tab" href="#menu3">Your Pitures</a></li>
		  </ul>

		  <div class="tab-content main-grids user_detail_mn">
			<?php 
			if(!empty($_SESSION['user_id']))
			{
				$user_id = $_SESSION['user_id'];
				$sql_user_detail = "SELECT * FROM users   where id='$user_id'";			
			}
				if(!empty($_SESSION['email']))
				{
					$loginUerEmail = $_SESSION['email'];
					$sql_user_detail = "SELECT * FROM users   where email= '$loginUerEmail'";			
				}
				
				
				//$sql_user_detail = "SELECT * FROM users   where id='$user_id' OR email= '$loginUerEmail'";
				$result_user_detail = $conn->query($sql_user_detail);
				if ($result_user_detail->num_rows>0)
				{
				while($user_details = $result_user_detail->fetch_assoc())
				{
			?>
			<div id="home" class="tab-pane fade in active user_detail">
			  <h3>Profile Details</h3>
					<div class="col-sm-3">
						<?php
							if(empty($user_details["image"]))
							  { 
							  //print"<pre>";print_r($_SESSION);print"</pre>";exit; 
							  ?>
								 <img src="admin/uploads/images/default.png" class="img-responsive" width="150" height="150">
							 <?php }
							  else
							  { ?>
								   <img src="admin/uploads/images/<?php echo $user_details['image']; ?>" class="img-responsive img-circle" width="150" height="150" >
							 <?php
							 }
						 ?>
					</div>
					<div class="col-sm-9">
					  <table  class="table table-responsive">    
						<tbody>
							<tr>
							  <th>Name</th>
							  <td><?php echo $user_details['name'];?></td>
							</tr>
                        <tr>
                            <th>Email</th>
                            <td><?php echo $user_details['email'];?></td>
                        </tr>
                        <tr>
                            <th>Phone Number</th>
                            <td><?php echo $user_details['phone'];?></td>
						</tr>
						<tr>
                            <th>Country</th>
                            <td><?php echo $user_details['country'];?></td>
						</tr>
						</tbody>
					</table>
					<a href="profile_edit.php?p_edit=<?php echo $user_details['id'];?>" style="color: #fff;"><button type="button" class="btn btn-success pull-right" style="margin-top: 10px" >Edit</button></a>
				</div>
			</div>
				<?php 
				} 
				}
				?>
			<div class="clearfix"></div>
			<div id="menu1" class="tab-pane fade user_detail">
				<div class="recommended">
					<div class="recommended-grids">
						<h3>Your Videos</h3>
					</div>
					<?php 
					$user_email = $_SESSION['email'];
					$sql_get = "SELECT * from users WHERE email='$user_email'";
					$result_get = $conn->query($sql_get);
					if ($result_get->num_rows>0)
					{
					$user=$result_get->fetch_assoc();
						
					$user_id = $user['id'];
                    $sql = "SELECT * FROM videos   where user_id='$user_id'";
                    $result = $conn->query($sql);
                    if ($result->num_rows>0)
                    {
                    while($category_list = $result->fetch_assoc())
                    {
                    ?>
					<div class="col-md-4 resent-grid recommended-grid slider-top-grids">
						<div class="resent-grid-img recommended-grid-img">
							<a href="videos.php?video_id=<?php echo $category_list['id'];?>&video_cat_id=<?php echo $category_list['categories'];?>"><img src="images/<?php echo $category_list['image'];?>" alt="" height="250px" /></a>
							<!--<div class="time">
								<p>3:04</p>
							</div>
							<div class="clck">
								<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
							</div>-->
							<div class="clck">
								<span class="glyphicon glyphicon-play-circle" aria-hidden="true"></span>
							</div>
						</div>
						<div class="resent-grid-info recommended-grid-info">
							<h3><a href="videos.php" class="title title-info"><?php echo $category_list['item_name'];?></a></h3>
							<ul>
								<li>
									<ul>
									<p class="author author-info"><a  class="<?php echo $category_list['id'];?>" onclick="div_show_video('deleteVideo',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a></p>
									</ul>
								</li>
								<li class="right-list"><p class="views views-info"><?php echo $category_list['view_count'];?> views</p></li>
							</ul>
						</div>
					</div>
					<?php 
                     	} 
                    	}
						}
                     ?>
					<div class="clearfix"> </div>
				</div>
			</div>
			<div id="menu2" class="tab-pane fade user_detail">
				<div class="recommended">
					<div class="recommended-grids">
						<div class="recommended-info">
							<h3>Your Gifs</h3>
						</div>
						<?php
						$user_email = $_SESSION['email'];
						$sql_get = "SELECT * from users WHERE email='$user_email'";
						$result_get = $conn->query($sql_get);
						if ($result_get->num_rows>0)
						{
						$user=$result_get->fetch_assoc();
					
						$user_id = $user['id'];
	                    $sql = "SELECT * FROM gifs  where user_id='$user_id'";
	                    $result = $conn->query($sql);
	                    if ($result->num_rows>0)
	                    {
	                    while($category_gifs = $result->fetch_assoc())
	                    {
	                    ?>
						<div class="col-md-3 resent-grid recommended-grid">
							<div class="resent-grid-img recommended-grid-img">
								<a href="gifs.php?gif_id=<?php echo $category_gifs['id'];?>&gif_cat_id=<?php echo $category_gifs['categories'];?>"><img src="images/<?php echo $category_gifs['image'];?>" alt="" width="320" height="180" /></a>
								<!--<div class="time small-time">
									<p>2:34</p>
								</div>
								<div class="clck small-clck">
									<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
								</div>-->
							</div>
							<div class="resent-grid-info recommended-grid-info video-info-grid">
								<h5><a href="gifs.php" class="title"><?php echo $category_gifs['item_name'];?></a></h5>
								<ul>
									<li><p class="author author-info"><a  class="<?php echo $category_gifs['id'];?>" onclick="div_show_gif('deleteGif',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a></p></li>
									<li class="right-list"><p class="views views-info"><?php echo $category_gifs['view_count'];?> views</p></li>
								</ul>
							</div>
						</div>
						<?php 
                     	} 
                    	}
						}
                     	?>
						<div class="clearfix"> </div>
					</div>
				</div>
			</div>
			<div id="menu3" class="tab-pane fade user_detail">
				<div class="recommended">
					<div class="recommended-grids">
						<div class="recommended-info">
							<h3>Your Pictures</h3>
						</div>
						<?php
						$user_email = $_SESSION['email'];
						$sql_get = "SELECT * from users WHERE email='$user_email'";
						$result_get = $conn->query($sql_get);
						if ($result_get->num_rows>0)
						{
						$user=$result_get->fetch_assoc();
						
						$user_id = $user['id'];
	                    $sql = "SELECT * FROM pictures  where user_id='$user_id'";
	                    $result = $conn->query($sql);
	                    if ($result->num_rows>0)
	                    {
	                    while($category_pics = $result->fetch_assoc())
	                    {
	                    ?>
						<div class="col-md-3 resent-grid recommended-grid">
							<div class="resent-grid-img recommended-grid-img">
								<a href="pictures.php?pics_id=<?php echo $category_pics['id'];?>&pic_cat_id=<?php echo $category_pics['categories'];?>"><img src="images/<?php echo $category_pics['image'];?>" alt="" width="320" height="180" /></a>
								<!--<div class="time small-time">
									<p>2:34</p>
								</div>
								<div class="clck small-clck">
									<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
								</div>-->
							</div>
							<div class="resent-grid-info recommended-grid-info video-info-grid">
								<h5><a href="gifs.php" class="title"><?php echo $category_pics['item_name'];?></a></h5>
								<ul>
									<li><p class="author author-info"><a  class="<?php echo $category_pics['id'];?>" onclick="div_show_pic('deletePic',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a></p></li>
									<li class="right-list"><p class="views views-info"><?php echo $category_pics['view_count'];?> views</p></li>
								</ul>
							</div>
						</div>
						<?php 
                     	} 
                    	}
						}
                     	?>
						<div class="clearfix"> </div>
				</div>
			  </div>
			</div>
		  </div>
			<!-- //container -->
			<div id="deleteVideo" style="display:none;">
			   <!-- Popup Div Starts Here -->
				<div id="popupVideo" class="popup">
					<!-- Contact Us Form -->
					<img id="close" src="images/close.png" onclick="div_hide_video('deleteVideo')" style="cursor:pointer;">
					<form method="post">
						<hr>
						<h2>Are You Sure??</h2>
						<input type="submit" name="deleteVideo" value="OK">
						<input type="hidden" name="id" id="deleteId">
					</form>
				</div>
			</div>
			
			<div id="deleteGif" style="display:none;">
			   <!-- Popup Div Starts Here -->
				<div id="popupGif" class="popup">
					<!-- Contact Us Form -->
					<img id="close" src="images/close.png" onclick="div_hide_gif('deleteGif')" style="cursor:pointer;">
					<form method="post">
						<hr>
						<h2>Are You Sure??</h2>
						<input type="submit" name="deleteGif" value="OK">
						<input type="hidden" name="id" id="deleteIdgif">
					</form>
				</div>
			</div>
			
			<div id="deletePic" style="display:none;">
			   <!-- Popup Div Starts Here -->
				<div id="popupVideo" class="popup">
					<!-- Contact Us Form -->
					<img id="close" src="images/close.png" onclick="div_hide_pic('deletePic')" style="cursor:pointer;">
					<form method="post">
						<hr>
						<h2>Are You Sure??</h2>
						<input type="submit" name="deletePic" value="OK">
						<input type="hidden" name="id" id="deleteIdPic">
					</form>
				</div>
			</div>
			
			<script>
			 function div_show_video(id,eid) {
					$("#deleteVideo").css('display', 'block');
					$("#deleteId").val(eid);
				}
			function div_hide_video(id){
				$("#deleteVideo").css('display', 'none');
				// document.getElementById('addProduct').style.display = "none";
				}	
			function div_show_gif(id,gid) {
					$("#deleteGif").css('display', 'block');
					$("#deleteIdgif").val(gid);
				}
			function div_hide_gif(id){
				$("#deleteGif").css('display', 'none');
				// document.getElementById('addProduct').style.display = "none";
				}	
			function div_show_pic(id,pid) {
					$("#deletePic").css('display', 'block');
					$("#deleteIdPic").val(pid);
				}
			function div_hide_pic(id){
				$("#deletePic").css('display', 'none');
				// document.getElementById('addProduct').style.display = "none";
				}					
			</script>
<!-- footer -->
<?php include "include/footer.php"; ?>			