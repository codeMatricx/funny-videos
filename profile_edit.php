 <?php 
 $pid = $_GET['p_edit'];
 ?>
<!--header-->
<?php include"include/header.php"; ?>
<?php


if (isset($_POST['upd'])) 
{
  if(isset($_FILES["image"]['name']) && !empty($_FILES["image"]['name']))
  {
      $target_dir = "admin/uploads/images/";
      $image=$_FILES["image"];
      $target_file = $target_dir . basename($_FILES['image']["name"]);
      $uploadOk = 1;
      $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION); 

          $image="";
          if(empty($_FILES["image"]["tmp_name"]))
          {
            echo "Please choose File!";
            $uploadOk = 0;
          }
          else
          {
                 if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                  && $imageFileType != "gif" ) 
                {
                  echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                  $uploadOk = 0;
                 }
           
                $check = getimagesize($_FILES["image"]["tmp_name"]);
                if($check != true) 
                {
                      echo "File is not an image.";
                      $uploadOk = 0;
                }
                else
                { 
               
                  if ($_FILES["image"]["size"] > 500000)
                  {
                    echo "Sorry, your file is too large.";
                    $uploadOk = 0;
                   }
                }
          }
            if($uploadOk == 1)
            {
              if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file))
              {
                $image =  $_FILES["image"]["name"];
                echo "The file " . "&nbsp" . "<b>" . basename( $_FILES["image"]["name"]). "</b>" . "&nbsp" . " has been uploaded.";
              }
            }

  }

$name=$_POST["name"];
$email=$_POST["email"];
$phone=$_POST["phone"];
$country=$_POST["country"];
if(isset($image) && !empty($image))
{
   $user_image = $image;
   $sql = "UPDATE users SET name = '$name', phone = '$phone' ,email='$email', country='$country', image='$user_image'  WHERE id = '$pid'";
}
else
{
  $sql = "UPDATE users SET name = '$name', phone = '$phone' ,email='$email', country='$country' WHERE id = '$pid'";
}
// $cat=$_POST["cat"];
// $subcat=$_POST["subcat"];
$status = 1;

        if ($conn->query($sql) === TRUE)
        {
           $responseMessage =  "Product edit successfully";

        }
        else
        {
            $responseMessage =  "Connection failed: " . $conn->connect_error;
        }
}

?>
 
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		  <div class="box-body">
              <form method="post" enctype="multipart/form-data">
                  <div id="example1" class="table table-bordered table-striped">
                  <table  class="table table-bordered table-striped">    
                    <tbody>
							<?php 
							$sql_get = "SELECT * from users WHERE id='$pid'";
							$result_get = $conn->query($sql_get);
							if ($result_get->num_rows>0)
							{
							$profile_edit=$result_get->fetch_assoc();
							//print_r($promotion);exit;
							?>
                        <tr>
                          <th>Name</th>
                          <td>
                            <input type="text" name="name" class="form-control" placeholder="Enter your name" value="<?php echo $profile_edit['name']; ?>">
                          </td>
                        </tr>
						<tr>
                          <th>Email</th>
                          <td>
                            <input type="text" name="email" class="form-control" placeholder="Enter your name" value="<?php echo $profile_edit['email']; ?>">
                          </td>
                        </tr>
						<tr>
                          <th>Mobile</th>
                          <td>
                            <input type="text" name="phone" class="form-control" placeholder="Enter your name" value="<?php echo $profile_edit['phone']; ?>">
                          </td>
                        </tr>
                        <tr>
                            <th>Image</th>
                            <td>
							  <?php
							    if(empty($profile_edit["image"]))
                                  { 
								  //print"<pre>";print_r($_SESSION);print"</pre>";exit; 
								  ?>
									 <img src="admin/uploads/images/default.png" class="img-responsive" width="50" height="50">
								 <?php }
								  else
								  { ?>
									   <img src="admin/uploads/images/<?php echo $profile_edit['image']; ?>" class="img-responsive img-circle" width="50" height="50" >
								 <?php
								 }
                                 ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Choose image</th>
                            <td><label for="newimage" class="btn btn-success text-muted text-center " style="width:30%;">Choose Image</label>
                            <input id="newimage" type="file" name="image" style="display:none"  >
                            <input type = "hidden" name = "image_first" id = "image_first" value = "<?php  echo $profile_edit['image'];?>">
                            </td>
                        </tr>
                        <tr>
                            <th>Country</th>
                            <td><input type="text" name="country" class="form-control" value = "<?php echo $profile_edit['country']; ?>"/></td>
                        </tr>
                              <?php }?>
                    </tbody>
                </table>
              </div>
              <a href="profile.php" style="color: #fff;"><button type="button" class="btn btn-success" style="margin: 10px" >Back</button></a>
              <button type="submit" class="btn btn-success  pull-right" name="upd" style="margin: 10px" >Update</button>
            </form>
            </div>
		  
<!-- footer -->
<?php include "include/footer.php"; ?>			