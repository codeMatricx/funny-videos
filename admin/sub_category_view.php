 <?php 
 include "database.php";

// $user_id = $_GET['uid'];

?>

<?php require('include/head.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php include "include/header.php";?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include "include/left_sidebar.php";?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Sub Category Details
        
      </h1>
      
    </section>

    <!-- Main content -->

    <section class="content">
      <!-- Info boxes -->

     <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
              <table  class="table table-bordered table-striped">
                <tbody>
                  <tr>
                    <th>Category Name</th>
                    <td>Indoor</td> 
                </tr>
                <tr>
                    <th>Sub Category Name</th>
                    <td>Appliances </td> 
                </tr>
                </tbody>
                
              </table>

              

              <a href="sub_category.php" style="color: #fff;"><button type="button" class="btn  " style="margin-top: 10px">Back</button></a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include "include/footer.php" ;?>
  

  <!-- Control Sidebar -->
  <?php include "include/right_sidebar.php" ;?>
  
  
</div>


  <div id="addNews">
                    

  <div class="flappy-dialog">

  <h2>Add Branch</h2>
  <img id="close" src="assets/img/close.png" onclick="div_hide('addNews')" style="float: right; margin-top: -63px; padding-right: 6px;">
  <form  id="form" method="post" name="form" enctype="multipart/form-data">
  
  <input id="name" name="name" placeholder="Branch Name" type="text">
  <input id="Price" name="price" placeholder="Branch Address" type="text">
    
  <div class="flappy-dialog-buttons" style="margin-top: 5px;">
    <div class="left-flap"></div>
    <input type="submit" name="sub" value="ADD">
    <div class="right-flap"></div>
  </div>
</form>
</div>
                        
                    </div>

 

                    <div id="deleteProduct">
                    <!-- Popup Div Starts Here -->
                    <div id="popupDelete" class="popup">
                        <!-- Contact Us Form -->
                        <img id="close" src="assets/img/close.png" onclick="div_hide('deleteProduct')">
                        <form method="post">
                            <hr>
                            <h2>Are You Sure??</h2>
                            <input type="submit" name="deleteProduct" value="OK">
                            <input type="hidden" name="id" id="deleteId">
                        </form>
                    </div>
 </div>
 



<?php include "include/footer_script.php" ;?>

</body>
</html>
