 <?php 
 include "database.php";

$vid = $_GET['video_id'];
?>

<?php require('include/head.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php include "include/header.php";?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include "include/left_sidebar.php";?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Items Details
        
      </h1>
      
    </section>

    <!-- Main content -->

    <section class="content">
      <!-- Info boxes -->

     <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
              <table  class="table table-bordered table-striped">
                <?php 
                            $sql = "SELECT * from videos WHERE id='$vid'";
                            $result = $conn->query($sql);
                            if ($result->num_rows>0)
                            {
                            $video_view=$result->fetch_assoc();
                            ?>
                <thead>
                <tr>
                  <th>Categories</th>
                  <?php 
                      $sql = "SELECT * from category";
                      $result = $conn->query($sql);
                      if ($result->num_rows>0)
                      {
                          while($category = $result->fetch_assoc())
                          {
                    if($category['id'] == $video_view['categories'])
                   { ?>
                      <td><?php echo $category['category_name'];  ?></td>
                  <?php }   
                    ?>
                  <?php
                   } } ?>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <th>Name</th>
                  <td><?php echo $video_view['item_name'];  ?></td>
                </tr>
                <tr>
                  <th>image</th>
                  <td><img src="uploads/images/<?php echo $video_view['image'];  ?>" width="50" height="50"></td>
                </tr>
                <tr>
                  <th>Videos</th>
                  <td>
                    <video width="100px" height="100px" controls>
                        <source src="uploads/videos/<?php echo $video_view['video'];  ?>" type="video/mp4">
                    </video>
                  </td>
                </tr> 
                <tr>
                  <th>Descrition</th>
                  <td><?php echo $video_view['description'];  ?></td>
                </tr>       
                </tbody>
              </table>
              <?php } ?>
              <a href="videos.php" style="color: #fff;"><button type="button" class="btn  " style="margin-top: 10px">Back</button></a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include "include/footer.php" ;?>
  <!-- Control Sidebar -->
  <?php include "include/right_sidebar.php" ;?>
</div>
<?php include "include/footer_script.php" ;?>
</body>
</html>
