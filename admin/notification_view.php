 <?php 
 include "database.php";
$id=$_GET['notification_id'];
?>
<?php require('include/head.php'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php include "include/header.php";?>
  <?php include "include/left_sidebar.php";?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Notification Details  
      </h1>
      
    </section>

    <!-- Main content -->

    <section class="content">
      <!-- Info boxes -->

     <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
              <table  class="table table-bordered table-striped">
                      <?php 
                      $sql = "SELECT n.device_type AS notification_type,n.message AS notification_message,u.name AS sender_name,u.id AS reciever_name from notifications AS n INNER JOIN users AS u ON n.sender_user_id=u.id WHERE n.id='$id'";
                      $result = $conn->query($sql);
                      if ($result->num_rows>0)
                      {
                      $notification = $result->fetch_assoc();
                      ?>
                <tbody>
                  <tr>
                    <th>Sender Name</th>
                    <td><?php echo  $commission_view['sender_name'];?></td> 
                 </tr>
                 <tr>
                    <th>Reciever Name</th>
                    <td><?php echo  $commission_view['reciever_name'];?></td> 
                </tr>
                <tr>
                    <th>Notification Message</th>
                    <td><?php echo  $commission_view['notification_message'];?></td> 
                </tr>
                <tr>
                    <th>Notification type</th>
                    <td><?php echo  $commission_view['notification_type'];?></td> 
                </tr>
                
                </tbody>
                 <?php } ?>
              </table>

              

              <a href="commission.php" style="color: #fff;"><button type="button" class="btn  " style="margin-top: 10px">Back</button></a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include "include/footer.php" ;?>
  <!-- Control Sidebar -->
  <?php include "include/right_sidebar.php" ;?> 
</div>
<?php include "include/footer_script.php" ;?>

</body>
</html>
