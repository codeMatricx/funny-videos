 <?php 
 include "database.php";
 //$user_id=$_SESSION['usr_id'];
if (isset($_POST['deleteProduct']))
{
    $id = ($_POST['id']);
    $sql = "DELETE FROM admin_commission WHERE id= '$id'";
    if ($conn->query($sql) === TRUE)
    {
       $responseMessage =  "Remove successful";
    }
    else
    {
       $responseMessage =  "Connection failed: " . $conn->error;
    }
}
?>

<?php 
if(isset($_POST["sub"]))
{
      $commission_value=$_POST["commission_value"];
      $commission_type_id=$_POST["commission_type_id"];

      $sql = "INSERT INTO admin_commission (commission_value,commission_type_id)
      VALUES ('$commission_value','$commission_type_id')";
      if ($conn->query($sql) === TRUE) 
      {
          header("location:commission.php");
      } 
      else
      {
          echo "Error: " . $sql . "<br>" . $conn->error;
      }
}
  ?>
<?php require('include/head.php'); ?>
<body onload="initialize()" class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php include "include/header.php";?> 
  <?php include "include/left_sidebar.php";?>
   

  <!-- Left side column. contains the logo and sidebar -->
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
          Commission Details 
          </h1>
          <ol class="breadcrumb">
          <li><button type="button" class="btn btn-block " style="margin-top: -5px;" onclick="div_show('addNews')" id="">Add Commission</button></li>
          </ol>
        </section>
<section class="content">
 <div class="box">
      <div class="box-header">
        <h3 class="box-title">Commission Table With Full Features</h3>
      </div>
       <div class="box-body table-responsive table-scroll-y">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>S.NO</th>
              <th>Commission Percent</th>
              <th>Commission Type</th>
               <!-- <th>Activate/Deactivate</th> -->
              <th>Status</th> 
              <th>Action</th>
              <th><input name="select-all" id="checkall" onClick="check_uncheck_checkbox(this.checked);" value="check_all" type="checkbox"></th>
            </tr>
            </thead>
            <tbody>
                    <?php 
                    $sql = "SELECT ac.commission_status,ac.commission_type_id,ac.id,ac.commission_value,d.discount_name AS commission_type from admin_commission AS ac  INNER JOIN discount_type AS d ON ac.commission_type_id = d.id Order By ac.id ";
                    $result = $conn->query($sql);
                    if ($result->num_rows>0)
                    {
                    $serial=0;
                    while($commission = $result->fetch_assoc())
                    {
                    $serial++;
                    ?>
              <tr id="<?php  echo $commission['id'];?>">
                <td><?php echo $serial; ?></td>
                <?php
                if($commission['commission_type_id']==1)
                {
                ?>
                <td><?php  echo $commission['commission_value']."%";?></td>
                <?php 
                }
                ?>
                <?php
                if($commission['commission_type_id']==2)
                {
                ?>
                <td><?php  echo $commission['commission_value'];?></td>
                <?php 
                }
                ?>
                <td><?php  echo $commission['commission_type'];?></td>
                
                <?php 
                  if($commission['commission_status'] == 1)
                  {
                ?>
                     <td><img src="assets/img/enable.gif" class="img-responsive" title="Activated"></td>
                  <?php
                  }
                 ?>
                 <?php
                 if($commission['commission_status'] == 0)
                  {
                ?>
                   <td><img src="assets/img/disable.gif" class="img-responsive" title="Deactivated"></td>
                 <?php
                  }
                 ?>
               
                <!-- <td><label class="switch">
                     <input type="checkbox" checked>
                     <span class="slider round"></span>
                     </label>
                </td> -->
                <td >
                  <a href="commission_edit.php?commission_id=<?php  echo $commission['id'];?>"  style="cursor: pointer;">
                  <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a>/
                  <a href="commission_view.php?commission_id=<?php  echo $commission['id'];?>"  style="cursor: pointer;"><i class="fa fa-eye" aria-hidden="true"></i></a><!-- /
                  <a class="<?php echo $commission['id'];?>" onclick="div_show('deleteProduct',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;"><i class="fa fa-trash-o" aria-hidden="true"></i></a> -->
                </td>
                <td><input   type="checkbox" value="<?php echo $commission['id'];?>" name="action" id="checkbox-1"></td>
              </tr> 
                   <?php 
                     } 
                    }
                     ?>
            </tbody>
              </table>
                <button class="btn btn-success" onclick="commissionActivate()">Activate</button>
                <button class="btn btn-danger" onclick="commissionDeactivate()">Deactivate</button>
                <button class="btn btn-danger" onclick="deleteCommission()">Delete</button>
            </div>
            <!-- /.box-body -->
 </div>
  <!-- /.content-wrapper -->
  <?php include "include/footer.php" ;?>
  <?php include "include/right_sidebar.php" ;?>  
</div>
<div id="addNews">                    
<div class="flappy-dialog">
<h2>Add Commission</h2>
<img id="close" src="assets/img/close.png" onclick="div_hide('addNews')" style="float: right; margin-top: -63px; padding-right: 6px;">
                <form  id="form" method="post" name="form" enctype="multipart/form-data">
                  <table width="100%">
                    
                    <tr>
                      <td align="right"><strong>Commission Type:</strong></td>
                      <td  align="left">
                      <input name="commission_type_id" value="1" checked="checked" onchange="show($(this).val())" type="radio">Percent&nbsp;
                      <input name="commission_type_id" value="2" onchange="show($(this).val())" type="radio"> Direct</td>
                    </tr>
                  </table>
                  <tr style="display: none;" id="commission_area">
                   <input id="percent" name="commission_value" placeholder="Commission Percent"  type="text" required>
                  </tr>
                <tr>
                   <input id="direct" name="commission_value" placeholder="Commission Direct"  type="text" style = "display: none;" required>
                </tr>
              <div class="flappy-dialog-buttons" style="margin-top: 5px;">
              <div class="left-flap"></div>
              <input type="submit" name="sub" value="ADD">
              <div class="right-flap"></div>
              </div>
              </form>
</div>
</div>

<div id="deleteProduct">
  <div id="popupDelete" class="popup">
  <img id="close" src="assets/img/close.png" onclick="div_hide('deleteProduct')">
  <form method="post">
  <hr>
  <h2>Are You Sure??</h2>
  <input type="submit" name="deleteProduct" value="OK">
  <input type="hidden" name="id" id="deleteId">
  </form>
  </div>
</div>
<?php include "include/footer_script.php" ;?>
<script src='http://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyAL1eAba9HoD7qszOJ-ggOvZvbq-TvXDys'></script>
<script src="js/script.js"> </script>
<script>
function show(val)
{
  if(val == 1)
  {
    $("#percent").show();
    $("#direct").hide();
  } 
    else
    {
      $("#direct").show();
      $("#percent").hide();
    } 
}
</script>
</body>
</html>
