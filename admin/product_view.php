 <?php 
 include "database.php";
 $user_id = $_GET['uid'];


?>

<?php require('include/head.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php include "include/header.php";?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include "include/left_sidebar.php";?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Product Details
        
      </h1>
      
    </section>

    <!-- Main content -->

    <section class="content">
      <!-- Info boxes -->

     <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
 <?php 
                                    $sql = "SELECT * from products WHERE id=$user_id";
                                    $result = $conn->query($sql);
                                    if ($result->num_rows>0)
                                    {
                                        $serial=1;
                                        
                                        if($products = $result->fetch_assoc())
                                        {
                                            
                                    ?>


              <table  class="table table-bordered table-striped">
                <thead>
                <tr>
        <th>Product Name</th>
        <td><?php  echo $products['name'];?></td>
        
      </tr>
      <tr>
        <th>Image</th>
        <td><img src="towfeerapi/uploads/<?php  echo $products['image'];?>" class="img-responsive" width="50px" height="50px"></td>
        
      </tr>
   
                </thead>
                <tbody>
                <tr>
        <th>Product Description</th>
        <td><?php  echo $products['description'];?></td>
        
      </tr>
      <tr>
        <th>Product Price</th>
        <td><?php  echo $products['price'];?></td>
        
      </tr>
      <tr>
        <th>Add Branch</th>
        <td><?php  echo $products['sub_categories'];?></td>
        
      </tr>
      <tr>
        <th>Add Brand</th>
        <td><?php  echo $products['categories'];?></td>
        
      </tr>
      <tr>
        <th>Availability</th>
        <td><?php  echo $products['availibility'];?></td>
        
      </tr>
      <tr>
        <th>Bidding Price</th>
        <td><?php  echo $products['bidding'];?></td>
        
      </tr>
                
               
                </tbody>
                
              </table>
<?php
                                        $serial++;
                                         } } ?>



              <a href="product.php" style="color: #fff;"><button type="button" class="btn  " style="margin-top: 10px">Back</button></a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include "include/footer.php" ;?>
  

  <!-- Control Sidebar -->
  <?php include "include/right_sidebar.php" ;?>
  
  
</div>
<!-- ./wrapper -->



  

<?php include "include/footer_script.php" ;?>
<script type="text/javascript">
$(function() {
    $('.multiselect-ui').multiselect({
        includeSelectAllOption: true
    });
});
</script>

</body>
</html>
 