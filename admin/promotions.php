 <?php 
 //session_start();
 include "database.php";
 //$user_id=$_SESSION['usr_id'];
if (isset($_POST['deleteProduct']))
{
    $id = ($_POST['id']);
    $sql = "DELETE FROM promotions WHERE id= $id";
    if ($conn->query($sql) === TRUE)
    { 
        $responseMessage =  "Remove successful";
    }
    else
    {
        $responseMessage =  "Connection failed: " . $conn->error;
    }
}

if (isset($_POST['sub']))
{
    $card_title = ($_POST['card_title']);
    $card_amount = ($_POST['card_amount']);

    $sql = "INSERT INTO promotions(card_title,card_amount) VALUES ('$card_title','$card_amount')";
    if ($conn->query($sql) === TRUE)
    { 
        $responseMessage =  "Insert Successful";
    }
    else
    {
        $responseMessage =  "Connection failed: " . $conn->error;
    }
}
?>

<?php require('include/head.php'); ?>
<body onload="initialize()" class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php include "include/header.php";?> 
  <?php include "include/left_sidebar.php";?>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
          <section class="content-header">
              <h1>
              Gift Cards Details
              </h1>
              <ol class="breadcrumb">
              <li><button type="button" class="btn btn-block " style="margin-top: -5px;" onclick="div_show('addNews')" id="">Add Gift Cards</button></li>
              </ol>
          </section>
    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
<div class="box">
<div class="box-header">
<h3 class="box-title">Gifts Table With Full Features</h3>
</div>
          <div class="box-body table-responsive table-scroll-y">
          <table id="example1" class="table table-bordered table-striped">
          <thead>
              <tr>
              <th>S.NO</th>
              <th>Card Title</th>
              <th>Card Amount</th>
              <th>Activate/Deactivate</th>
              <th>Status</th>
              <th>Action</th>
              <th><input name="select-all" id="checkall" onClick="check_uncheck_checkbox(this.checked);" value="check_all" type="checkbox"></th>
              </tr>
          </thead>
          <tbody>
              <?php 
              $sql = "SELECT * from promotions";
              $result = $conn->query($sql);
              if ($result->num_rows>0)
              {
              $serial=0;
              while($promotion = $result->fetch_assoc())
              {
              $serial++;
              ?>
              <tr id="<?php  echo $promotion['id'];?>">
              <td><?php echo $serial; ?></td>
              <td><?php  echo $promotion['card_title'];?></td>
              <td><?php  echo $promotion['card_amount'];?></td>
              <td><label class="switch">
              <input type="checkbox" checked>
              <span class="slider round"></span>
              </label>
              </td>
              <td><img src="assets/img/enable.gif" class="img-responsive" title="Activated"></td>
              <td>
              <a class="<?php echo $promotion['id'];?>" onclick="div_show('deleteProduct',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
              </td>
              <td><input   type="checkbox" name="action" id="checkbox-1"></td>
              </tr> 
              <?php 
              } }
              ?>
          </tbody>
          </table>
          <button class="btn btn-success">Activate</button>
          <button class="btn btn-danger">Deactivate</button>
          <button class="btn btn-danger">Delete</button>
          </div>
</div>
</div>
<?php include "include/footer.php" ;?>
<?php include "include/right_sidebar.php" ;?>
</div>
        <div id="addNews">                   
            <div class="flappy-dialog">
            <h2>Add Category</h2>
            <img id="close" src="assets/img/close.png" onclick="div_hide('addNews')" style="float: right; margin-top: -63px; padding-right: 6px;">
            <form  id="form" method="post" name="form" enctype="multipart/form-data">
            <input id="title" name="card_title" placeholder="Card Title" type="text" required>  
            <input id="amount" name="card_amount" placeholder="Card Amount (In $)" type="text" required>
            <div class="flappy-dialog-buttons" style="margin-top: 5px;">
            <div class="left-flap"></div>
            <input type="submit" name="sub" value="ADD">
            <div class="right-flap"></div>
            </div>
            </form>
            </div>
        </div>
        <div id="deleteProduct">
            <div id="popupDelete" class="popup">
            <img id="close" src="assets/img/close.png" onclick="div_hide('deleteProduct')">
            <form method="post">
            <hr>
            <h2>Are You Sure??</h2>
            <input type="submit" name="deleteProduct" value="OK">
            <input type="hidden" name="id" id="deleteId">
            </form>
            </div>
        </div>
<?php include "include/footer_script.php" ;?>
<script src='http://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyAL1eAba9HoD7qszOJ-ggOvZvbq-TvXDys'></script>
<script src="js/script.js"> </script>
</body>
</html>
