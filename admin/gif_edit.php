 <?php 
 include "database.php";
$gid = $_GET['gif_id'];
?>

<?php
if (isset($_POST['upd'])) 
{
  
if(empty($_FILES['image']['name']))
        {
        $image = $_POST['image_first'];
        }
        else
        {
        $target_dir = "products/images/";
        $target_file = $target_dir . basename($_FILES["image"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
        $image =  $_FILES["image"]["name"];
        $uploadOk = 1;
        }  

   //print"<pre>";print_r($_POST);print"</pre>";
 $categories=$_POST["categories"];
$name=$_POST["name"];
$description=$_POST["description"];

$status = 1;


 $sql = "UPDATE gifs SET categories = '$categories',item_name = '$name', image   = '$image', description='$description' WHERE id = $gid";
  
 //print_r($sql);exit;
        if ($conn->query($sql) === TRUE)
        {
           $responseMessage =  "Gif edit successfully";
           header('Location: gifs.php');
        }
        else
        {
            $responseMessage =  "Connection failed: " . $conn->connect_error;
        }
}
?>
<?php require('include/head.php'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php include "include/header.php";?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include "include/left_sidebar.php";?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gif Details
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
     <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <form method="post" enctype="multipart/form-data">
                  <div id="example1" class="table table-bordered table-striped">
                  <table  class="table table-bordered table-striped">    
                    <tbody>
                                <?php 
                                $sql_get = "SELECT * from gifs WHERE id='$gid'";
                                $result_get = $conn->query($sql_get);
                                if ($result_get->num_rows>0)
                                {
                                $gif_edit=$result_get->fetch_assoc();
                                //print_r($promotion);exit;
                                ?>
                        <tr>
                            <th><strong>Category:</strong></th>
                            <td>
                              <select name="categories" style="margin-left:0px;width: 50%;padding-left: 10px">
                              <?php 
                                $sql = "SELECT * from category";
                                $result = $conn->query($sql);
                                if ($result->num_rows>0)
                                {
                                    while($category = $result->fetch_assoc())
                                    {
                                     if($category['id'] == $gif_edit['categories'])
                                     { ?>
                                     <option value="<?php echo $category['id']; ?>" selected><?php echo $category['category_name']; ?></option>

                                     <?php }
                                      else
                                      { ?>
                                        
                                        <option value="<?php echo $category['id']; ?>" ><?php echo $category['category_name']; ?></option>

                                      <?php }   
                                ?>
                              <?php
                               } } ?>
                            </select>
                            </td>
                        </tr>
                        <tr>
                          <th>Name</th>
                          <td>
                            <input type="text" name="name" value="<?php echo $gif_edit['item_name']; ?>">
                          </td>
                        </tr>
                        <tr>
                            <th>image</th>
                            <td><img src="uploads/images/<?php echo $gif_edit['image']; ?>" width="50" height="50">
                            </td>
                        </tr>
                        <tr>
                            <th>Choose image</th>
                            <td><label for="newimage" class="btn text-muted text-center " style="width:30%;">Choose Image</label>
                            <input id="newimage" type="file" name="image" style="display:none"  >
                            <input type = "hidden" name = "image_first" id = "image_first" value = "<?php  echo $gif_edit['image'];?>">
                            </td>
                        </tr>
                        <tr>
                            <th>Description</th>
                            <td><input type="text" name="description" value = "<?php echo $gif_edit['description']; ?>"/></td>
                        </tr>
                              <?php }?>
                    </tbody>
                </table>
              </div>
              <a href="gifs.php" style="color: #fff;"><button type="button" class="btn" style="margin-top: 10px" >Back</button></a>
              <button type="submit" class="btn   pull-right" name="upd" style="margin-top: 10px" >Update</button>
            </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
    <!-- /.content -->
  </section>
  </div>
  <!-- /.content-wrapper -->
  <?php include "include/footer.php" ;?>
  <!-- Control Sidebar -->
  <?php include "include/right_sidebar.php" ;?>
</div>
<?php include "include/footer_script.php" ;?>
</body>
</html>
