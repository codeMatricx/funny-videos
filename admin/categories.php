 <?php 
 include "database.php";
if(isset($_POST["sub"]))
{
$category_name=$_POST["category_name"];

            $sql = "INSERT INTO category (category_name)
            VALUES ('$category_name')";
            if ($conn->query($sql) === TRUE) 
            {
            echo "New record created successfully";
            } 
            else
            {
            echo "Error: " . $sql . "<br>" . $conn->error;
            }
}

    if (isset($_POST['deleteProduct']))
    {
            $id = ($_POST['id']);
            $sql = "DELETE FROM category WHERE id= $id";
            if ($conn->query($sql) === TRUE)
            {
            $responseMessage =  "User Remove successfully";
            }
            else
            {
            $responseMessage =  "Connection failed: " . $conn->error;
            }
   }
   ?>

<?php require('include/head.php'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php include "include/header.php";?>
  <?php include "include/left_sidebar.php";?>
  <div class="content-wrapper">
        <section class="content-header">
        <h1>
        Dashboard  
        </h1>
        <ol class="breadcrumb">
        <li><button type="button" class="btn btn-block " style="margin-top: -5px;" onclick="div_show('addNews')" id="">Add Category</button></li>
        </ol>
        </section>
    <section class="content">
    <div class="box">
      <div class="box-header">
      <h3 class="box-title">Category Table With Full Features</h3>
      </div>
      <div class="box-body table-responsive table-scroll-y">
      <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                  <th>S.NO</th>
                  <th>Category Name </th>
                  <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>               
                      <?php 
                      $sql = "SELECT * from category";
                      $result = $conn->query($sql);
                      if ($result->num_rows>0)
                      {
                      $serial=1;
                      while($categories = $result->fetch_assoc())
                      {
                      ?>
                      <tr id="<?php  echo $categories['id'];?>">
                      <td><?php echo $serial; ?></td>
                      <td><?php  echo $categories['category_name'];?></td>
                      <td >
                      <a href="categories_edit.php?cat_id=<?php echo $categories['id'];?>"  style="cursor: pointer;">
                      <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a>/
                      <a href="categories_view.php?uid=<?php echo $categories['id'];?>"  style="cursor: pointer;"><i class="fa fa-eye" aria-hidden="true"></i></a>/
                      <a class="<?php  echo $categories['id'];?>" onclick="div_show('deleteProduct',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                      </tr>
                      <?php
                      } } 
                      ?>               
                </tbody>               
          </table>
          </div>
          </div>
          </div>
<?php include "include/footer.php" ;?>
<?php include "include/right_sidebar.php" ;?>  
</div>
<div id="addNews">                   
  <div class="flappy-dialog">
            <h2>Add Category</h2>
            <img id="close" src="assets/img/close.png" onclick="div_hide('addNews')" style="float: right; margin-top: -63px; padding-right: 6px;">
            <form  id="form" method="post" name="form" enctype="multipart/form-data">
            <input id="categories" name="category_name" placeholder="Category Name" type="text" required>  
            <div class="flappy-dialog-buttons" style="margin-top: 5px;">
            <div class="left-flap"></div>
            <input type="submit" name="sub" value="ADD">
            <div class="right-flap"></div>
            </div>
            </form>
    </div>
</div>
      <div id="deleteProduct">
            <div id="popupDelete" class="popup">
            <img id="close" src="assets/img/close.png" onclick="div_hide('deleteProduct')">
            <form method="post">
            <hr>
            <h2>Are You Sure??</h2>
            <input type="submit" name="deleteProduct" value="OK">
            <input type="hidden" name="id" id="deleteId">
            </form>
            </div>
        </div>
<?php include "include/footer_script.php" ;?>
</body>
</html>
