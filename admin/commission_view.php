 <?php 
 include "database.php";
$id=$_GET['commission_id'];
?>
<?php require('include/head.php'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php include "include/header.php";?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php include "include/left_sidebar.php";?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Commission Details
        
      </h1>
      
    </section>

    <!-- Main content -->

    <section class="content">
      <!-- Info boxes -->

     <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
              <table  class="table table-bordered table-striped">
                         <?php 
                            $sql = "SELECT ac.id,ac.commission_value,d.discount_name AS commission_type from admin_commission AS ac  INNER JOIN discount_type AS d ON ac.commission_type_id = d.id WHERE ac.id='$id'";
                            $result = $conn->query($sql);
                            if ($result->num_rows>0)
                            {
                            $commission_view=$result->fetch_assoc();                          
                            ?>
                <tbody>
                  <tr>
                    <th>Commission</th>
                    <td><?php echo  $commission_view['commission_value'];?></td> 
                 </tr>
                 <tr>
                    <th>Commission Type</th>
                    <td><?php echo  $commission_view['commission_type'];?></td> 
                </tr>
                
                </tbody>
                 <?php } ?>
              </table>

              

              <a href="commission.php" style="color: #fff;"><button type="button" class="btn  " style="margin-top: 10px">Back</button></a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include "include/footer.php" ;?>
  <!-- Control Sidebar -->
  <?php include "include/right_sidebar.php" ;?> 
</div>
<?php include "include/footer_script.php" ;?>

</body>
</html>
