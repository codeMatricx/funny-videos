<?php

include "database.php";
$usr_id = $_GET['uid'];
?>
<?php include "include/header.php";?>




<?php



if (isset($_POST['upd'])) 
{
$uname=$_POST["uname"];

 $type=$_POST["type"];
if(isset($_FILES["image"]['name']) && !empty($_FILES["image"]['name']))
  {
      $target_dir = "towfeerapi/uploads/";
      $image=$_FILES["image"];
      $target_file = $target_dir . basename($_FILES['image']["name"]);
      $uploadOk = 1;
      $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION); 

          $image="";
          if(empty($_FILES["image"]["tmp_name"]))
          {
            echo "Please choose File!";
            $uploadOk = 0;
          }
          else
          {
                 if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                  && $imageFileType != "gif" ) 
                {
                  echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                  $uploadOk = 0;
                 }
           
                $check = getimagesize($_FILES["image"]["tmp_name"]);
                if($check != true) 
                {
                      echo "File is not an image.";
                      $uploadOk = 0;
                }
                else
                { 
               
                  if ($_FILES["image"]["size"] > 500000)
                  {
                    echo "Sorry, your file is too large.";
                    $uploadOk = 0;
                   }
                }
          }
            if($uploadOk == 1)
            {
              if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file))
              {
                $image =  $_FILES["image"]["name"];
                echo "The file " . "&nbsp" . "<b>" . basename( $_FILES["image"]["name"]). "</b>" . "&nbsp" . " has been uploaded.";
              }
            }

 
 
 

$uid = $_SESSION['usr_id'];
   
if(isset($image) && !empty($image))
{
   $user_image = $image;
    $sql = "UPDATE brand SET name ='$uname' ,establish ='$type', image='$image' WHERE  user_id='$uid' AND id='$usr_id'";
}
 }
else{
$sql = "UPDATE brand SET name ='$uname' ,establish ='$type' WHERE  user_id='$uid' AND id='$usr_id'";
}
$status = 1;
        if ($conn->query($sql) === TRUE)
        {
           $responseMessage =  "Product edit successfully";

        }
        else
        {
            $responseMessage =  "Connection failed: " . $conn->connect_error;
        }
}


?>
<?php require('include/head.php'); ?>


<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  
 
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include "include/left_sidebar.php";?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Brand Details/Update 
        
      </h1>
      
    </section>

    <!-- Main content -->

    <section class="content">
      <!-- Info boxes -->

     <div class="box">
            <div class="box-header">
              <h3 class="box-title">Brand Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form method="post" enctype="multipart/form-data">
                <?php 
                                    $sql = "SELECT * from brand WHERE  user_id=$uid AND id='$usr_id'";
                                    $result = $conn->query($sql);
                                    if ($result->num_rows>0)
                                    {
                                        $serial=1;
                                        
                                        if($brand = $result->fetch_assoc())
                                        {
                                            
                                   ?>


<table  class="table table-bordered table-striped">

 <thead>
                <tr>
        <th>Brand Name</th>
        <td><input type="text" name="uname" value="<?php  echo $brand['name'];?>" required /></td>
        
      </tr>
   
                </thead>

<tbody>
<tr>
        <th>Image</th>
        <td>
         
          
          <img src="towfeerapi/uploads/<?php echo $brand['image']; ?>" class="img-responsive" width="100px" height="100px">
           <label for="newimage" class="btn text-muted text-center " style="width:29%; margin-top: 2%;">Choose Image</label>
          <input id="newimage" type="file" name="image" style="display: none;" >


        </td>
        <tr>
        <th>Established In</th>
        <td><input type="text" name="type" value="<?php  echo $brand['establish'];?>" placeholder="(YYYY)" maxlength="4"; required /></td>
        
      </tr>   
      </tr>
</tbody>

</table>

<?php
                                        $serial++;
                                         } } ?>
                <a href="brand.php" style="color: #fff;"><button type="button" class="btn  " style="margin-top: 10px" >Back</button></a>
              <button type="submit" class="btn   pull-right" name="upd" style="margin-top: 10px" >Update</button>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
      
      
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <?php include "include/footer.php" ;?>
  
  
  <!-- Control Sidebar -->
  <?php include "include/right_sidebar.php" ;?>
  
</div>
<!-- ./wrapper -->

<?php include "include/footer_script.php" ;?>




</body>
</html>
