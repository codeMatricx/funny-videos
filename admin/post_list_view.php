 <?php 
 include "database.php";
 $id = $_GET['post_id'];
?>
<?php require('include/head.php'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php include "include/header.php";?>
<?php include "include/left_sidebar.php";?>
  <div class="content-wrapper">
      <section class="content-header">
      <h1>
      Post Details   
      </h1>   
      </section>
     <section class="content">
      <div class="box">
        <div class="box-body">
          <table  class="table table-bordered table-striped">
            <tbody>
                  <?php 
                  $sql = "SELECT p.status,p.lat,p.lng,p.id,u.name AS user_name,p.title,p.address,p.price,p.created_date_time,p.description,c.category_name,sc.sub_category_name,l.length_of_rental from post AS p INNER JOIN category AS c ON p.category=c.id INNER JOIN sub_category AS sc ON p.sub_category_id=sc.id INNER JOIN length_of_rental AS l ON p.length_of_rental=l.id INNER JOIN users AS u ON p.user_id=u.id  WHERE p.id='$id' Order By p.id";
                  //print_r($sql);exit;
                  $result = $conn->query($sql);
                  if ($result->num_rows>0)
                  {
                  $serial=0;
                  $post_view = $result->fetch_assoc();
                  
                  //For Post Gallary
                   $sql_gallary = "SELECT post_image FROM post_gallery WHERE post_id='$id'";
                    //print_r($sql_gallary);exit;
                  $result_gallary = $conn->query($sql_gallary);
                   if ($result_gallary->num_rows>0)
                   {
                     while($image_gallary = $result_gallary->fetch_assoc())
                     {

  

                  ?>
                <tr>
                  <th>User Name</th>
                  <td><?php  echo $post_view['user_name'];?></td> 
                </tr>
                <tr>
                  <th>Post Name</th>
                  <td><?php  echo $post_view['title'];?></td> 
                </tr>
                <tr>
                  <th>Category</th>
                  <td><?php  echo $post_view['category_name'];?></td> 
                </tr>
                <tr>
                  <th>SubCategory</th>
                  <td><?php  echo $post_view['sub_category_name'];?></td> 
                </tr>
                <tr>
                  <th>Address</th>
                  <td><?php  echo $post_view['address'];?></td> 
                </tr>
                <tr>
                  <th>Price</th>
                  <td><?php  echo $post_view['price'];?></td> 
                </tr>
                <tr>
                  <th>User Image</th>
                  <td><img src="assets/img/uploads/users/<?php echo $post_view['user_image']; ?>" class="img-responsive" style="width:30px; height:30px" ></td> 
                </tr>
                <tr>
                  <th>Post Date</th>
                  <td><?php  echo $post_view['created_date_time'];?></td> 
                </tr>
                <tr>
                  <th>Description</th>
                  <td><?php  echo $post_view['description'];?></td> 
                </tr>
                <tr>
                  <th>Length Of Rental</th>
                  <td><?php  echo $post_view['length_of_rental'];?></td> 
                </tr>
                <tr>
                  <th>Latitude</th>
                  <td><?php  echo $post_view['lat'];?></td> 
                </tr>
                <tr>
                  <th>Longitude</th>
                  <td><?php  echo $post_view['lng'];?></td> 
                </tr>

                <tr>
                  <th>Product Image</th>
                  <td><img src="assets/img/uploads/products/<?php echo $image_gallary['post_image'];?>" class="img-responsive" style="width:30px; height:30px" ></td> 
                </tr>
                <?php } } ?>

                <?php }?>
                </tbody>  
              </table>
              <a href="post_list.php" style="color: #fff;"><button type="button" class="btn  " style="margin-top: 10px">Back</button></a>
          </div>
      </div>
  </div>
  <?php include "include/footer.php" ;?>
  <?php include "include/right_sidebar.php" ;?>
</div>  
<?php include "include/footer_script.php" ;?>
</body>
</html>
