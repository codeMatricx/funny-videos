<?php require('include/head.php'); 
?>
 <?php 
include "database.php";


if (isset($_POST["deleteProduct"]))
{
    $id = ($_POST['id']);
    
    $sql = "DELETE FROM products WHERE id= $id";
    if ($conn->query($sql) === TRUE)
    {
        //unlink($image_dir.$deleteimage);
        $responseMessage =  "User Remove successfully";
    }
    else
    {
        $responseMessage =  "Connection failed: " . $conn->error;
    }
}

?>



<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php include "include/header.php";?>
<?php
if(isset($_POST["sub"]))
{
$target_dir = "towfeerapi/uploads/";
$image=$_FILES["image"];
$target_file = $target_dir . basename($_FILES['image']["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

$name=$_POST["name"];
$description=$_POST["description"];
$price=$_POST["product_price"];
$size=$_POST["size"];
$subcat=$_POST["branch"];
$categories=$_POST['categories'];
$availibility=$_POST['availibility'];
$bidding=$_POST['bidding'];

$status = 1;
$image="";
   if(empty($_FILES["image"]["tmp_name"]))
  {
    echo "Please choose File!";
    $uploadOk = 0;
  }
  else
  {
     if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
      && $imageFileType != "gif" ) 
   {
      echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
      $uploadOk = 0;
     }
   
    $check = getimagesize($_FILES["image"]["tmp_name"]);
    if($check != true) 
  {
        echo "File is not an image.";
        $uploadOk = 0;
    }
    else
  { 
   
  
     if ($_FILES["image"]["size"] > 500000)
   {
      echo "Sorry, your file is too large.";
      $uploadOk = 0;
     }
  }
  }
  
  if($uploadOk == 1)
  {
  if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file))
  {
     $image =  $_FILES["image"]["name"];
       //echo "The file " . "&nbsp" . "<b>" . basename( $_FILES["image"]["name"]). "</b>" . "&nbsp" . " has been uploaded.";
    }
    }
  $uid = $_SESSION['usr_id'];
  $sql = "INSERT INTO products (name,description, price, image, sub_categories,categories,bidding,availibility,size,user_id)
VALUES ('$name','$description','$price','$image','$subcat','$categories','$bidding','$availibility','$size','$uid')";



if ($conn->query($sql) === TRUE) 
{
  $product_id=mysqli_insert_id($conn);
  $sql_size = "INSERT INTO product_size (product_id, branch_id, brand_id, size_id,total_pices)
             VALUES ('$product_id','$subcat','$categories','$size','$availibility')";
    $conn->query($sql_size);
    
    $avilable_product_branch = "INSERT INTO avilable_product_branch (product_id, branch_id, product_count, size_id, user_id)
             VALUES ('$product_id','$subcat','$availibility','$size','$uid')";
    $hit=$conn->query($avilable_product_branch);
    echo "New record created successfully";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}



}

?>  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include "include/left_sidebar.php";?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Product Details
        
      </h1>
      <ol class="breadcrumb">
        <li><button type="button" class="btn btn-block " style="margin-top: -5px;" onclick="div_show('addNews')" id="">Add Product</button></li>
      </ol>
    </section>

    <!-- Main content -->

    <section class="content">
      <!-- Info boxes -->

     <div class="box">
            <div class="box-header">
              <h3 class="box-title">Product Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive table-scroll-y">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Product Name</th>
                  <th>Image</th>
                  <th>Product Description</th>
                  <th>Product Price</th>
                  <th> Brand</th>
                  <th> Branch</th>
                  <th>Availability</th>
                  <th>Bidding Price</th>
                   <th>Size</th>
                  <th>Action</th>
                </tr>
                </thead>
 <tbody>
                <?php 
                                    $uid = $_SESSION['usr_id'];
                                    $sql = "SELECT  p.*,b.name AS brand_name,brn.name AS branch_name,sn.size_name,sn.id AS size_id,brn.lat,brn.lng,u.id AS seller_id,u.name AS seller_name FROM products AS p INNER JOIN brand AS b ON b.id = p.categories INNER JOIN branch AS brn ON brn.id = p.sub_categories INNER JOIN size_name AS sn ON sn.id = p.size INNER JOIN users AS u ON u.id = p.user_id WHERE p.user_id = '$uid'";
                                    $result = $conn->query($sql);
                                    if ($result->num_rows>0)
                                    {
                                        $serial=1;
                                        
                                        while($products = $result->fetch_assoc())
                                        {
                                            
                                    ?>
               
                <tr id="<?php  echo $products['id'];?>">
                  <td><?php echo $serial; ?></td>
                  <td><?php  echo $products['name'];?></td>
                  <td><img src="towfeerapi/uploads/<?php echo $products['image']; ?>" class="img-responsive" width="30px" height="30px"></td>
                  <td><?php  echo $products['description'];?></td>
                  <td><?php  echo $products['price'];?></td>
                  <td><?php  echo $products['brand_name'];?></td>
                  <td><?php  echo $products['branch_name'];?></td>
                  <td><?php  echo $products['availibility'];?></td>
                  <td><?php  echo $products['bidding'];?></td>
                  <td><?php  echo $products['size_name'];?></td>
                  <td> 
                    <a href="product_edit.php?uid=<?php echo $products['id'];?>"  style="cursor: pointer;">
                     <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a>/
                     <a href="product_view.php?uid=<?php echo $products['id'];?>"  style="cursor: pointer;"><i class="fa fa-eye" aria-hidden="true"></i></a>/
                   <a class="<?php echo $products['id'];?>" onclick="div_show('deleteProduct',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                </tr>
                
               
                
                
                <?php
                                        $serial++;
                                         } } ?>

                                         </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include "include/footer.php" ;?>
  

  <!-- Control Sidebar -->
  <?php include "include/right_sidebar.php" ;?>
  
  
</div>
<!-- ./wrapper -->



  <div id="addNews">
                    

  <div class="flappy-dialog">

  <h2>Add Product</h2>
  <img id="close" src="assets/img/close.png" onclick="div_hide('addNews')" style="float: right; margin-top: -63px; padding-right: 6px;">
  <form  id="form" method="post" name="form" enctype="multipart/form-data">
  
  <input id="name" name="name" placeholder="Product Name" type="text" required>
  <label for="newimage" class="btn text-muted text-center " style="width:82%;margin-top: 2%;">Choose Image</label>
   <input id="newimage" type="file" name="image" style="display:none" required>
  <!-- <textarea class="form-control" name="p" rows="5" id="comment" placeholder="Product Description" style="width:82%;margin-left:9%;margin-top: 2%; " required></textarea> -->
  <input id="product_description" name="description" placeholder="Product Description" type="text" required >
 
  <input id="product_price" name="product_price" placeholder="Product Price" type="number" required >
     
<select id="ddlCars" name="categories" class="multiselect-ui form-control" multiple="multiple" style="color:#cdcdce;font-size: 18px;line-height: 2;width:80%;" required>
     <?php 
          $sql = "SELECT * from brand WHERE user_id = '$uid'";
          $result = $conn->query($sql);
          if ($result->num_rows>0)
          {
              $serial=1;
              
              while($brand = $result->fetch_assoc())
              {
                  
          ?>
          <option value="<?php echo  $brand['id'];?>"><?php  echo $brand['name'];?></option>
   <?php
  $serial++;
   } } ?>
   </select> 
   <select id="ddlCars3" name="branch" class="multiselect-ui form-control" multiple="multiple" style="color:#cdcdce;font-size: 18px;line-height: 2;width:80%;" required>
            
           <?php 
                                    $sql = "SELECT * from branch WHERE user_id = '$uid'";
                                    $result = $conn->query($sql);
                                    if ($result->num_rows>0)
                                    {
                                        $serial=1;
                                        
                                        while($branch = $result->fetch_assoc())
                                        {
                                            
                                    ?>
   
            <option value="<?php echo $branch['id']; ?>"><?php  echo $branch['name'];?></option>
           
        
   <?php
                                        $serial++;
                                         } } ?>
            
        </select> 


  <input id="availibility" name="availibility" placeholder=" Availibility" type="number" min="1" required >
  <select id="ddlCars4" name="size" class="multiselect-ui form-control" multiple="multiple" style="color:#cdcdce;font-size: 18px;line-height: 2;width:80%;" required>
    <option value="1">S</option>
    <option value="2">M</option>
    <option value="3">L</option>
    <option value="4">XL</option>
  </select>
  
  
  <div class="flappy-dialog-buttons" style="margin-top: 5px;">
    <div class="left-flap"></div>
    <input type="submit" name="sub" value="ADD">
    <div class="right-flap"></div>
  </div>
</form>
</div>
                        
                    </div>

 

                    <div id="deleteProduct">
                    <!-- Popup Div Starts Here -->
                    <div id="popupDelete" class="popup">
                        <!-- Contact Us Form -->
                        <img id="close" src="assets/img/close.png" onclick="div_hide('deleteProduct')">
                        <form  id="form" method="post" name="form" enctype="multipart/form-data">
                            <hr>
                            <h2>Are You Sure??</h2>
                            <input type="submit" name="deleteProduct" value="OK">
                            <input type="hidden" name="id" id="deleteId">
                        </form>
                    </div>
 </div>




<?php include "include/footer_script.php" ;?>
<script type="text/javascript">
  
  $(document).ready(function() {
     $('#ddlCars').multiselect();
      $('#ddlCars1').multiselect({ 
         numberDisplayed: 2,
          
     });
       
        $('#ddlCars3').multiselect({  
           nonSelectedText :'Select Branch'
           
     });
        $('#ddlCars4').multiselect({  
           nonSelectedText :'Select Size'
           
     });
});
</script>

</body>
</html>
