<?php 
include "database.php";
?>
<?php
$cat_id=$_GET['cat_id'];
if (isset($_POST["upd"])) 
{     
     if(empty($_FILES['image']['name']))
        {
        $image = $_POST['image_first'];
        }
        else
        {
        $target_dir = "assets/img/uploads/category/";
        $target_file = $target_dir . basename($_FILES["image"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
        $image =  $_FILES["image"]["name"];
        $uploadOk = 1;
        }

      $category_name=$_POST["category_name"];
      $color=$_POST["color"];
          $sql = "UPDATE category SET category_name = '$category_name', image = '$image', color_code='$color' WHERE id='$cat_id'";
          if ($conn->query($sql) === TRUE)
          {
          header("location:category.php");
          }
          else
          {
          $responseMessage =  "Connection failed: " . $conn->connect_error;
          }
}
?>
<?php require('include/head.php'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 <?php include "include/header.php";?> 
  <?php include "include/left_sidebar.php";?>
       <div class="content-wrapper">
            <section class="content-header">
              <h1>
              Category Edit/Update       
              </h1>
            </section>
            <section class="content">
        <div class="box">
        <div class="box-body table-responsive table-scroll-y">
        <form method="post" enctype="multipart/form-data">
                  <?php 
                  $sql = "SELECT * from category WHERE id='$cat_id'";
                  $result = $conn->query($sql);
                  if ($result->num_rows>0)
                  {
                  $categories = $result->fetch_assoc();
                  
                  ?>
            <table id="example1" class="table table-bordered table-striped">
            <table  class="table table-bordered table-striped">           
            <tbody>
            <tr>
              &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button class="jscolor
              {valueElement:'valueInput', styleElement:'styleInput'}">
              Choose Color
              </button>
              <p>
              <td>Value: <input id="valueInput" name="color" value="<?php echo $categories['color_code']; ?>"><br></td>
              <td>Style: <input id="styleInput"></td>
            </tr>
            <tr>
            <th>Category Name</th>
            <td><input type="text" name="category_name" value="<?php  echo $categories['category_name'];?>"/></td> </tr>
            <tr>
              <th>Category Icon</th>
              <td><img src="assets/img/uploads/category/<?php echo $categories['image']; ?>" class="img-responsive" style="width:30px; height:30px" ></td>
              
            </tr>
            <tr>
              <th>Change Icon</th>
                <td><label for="newimage" class="btn text-muted text-center btn-success" style="width:20%;margin-top: -4px;padding: 12px;">Icon</label>
                <input id="newimage" type="file" name="image" style="display: none;">
                <input type = "hidden" name = "image_first" id = "image_first" value = "<?php  echo $categories['image'];?>">
              </td>
            </tr>
            </tbody>             
            </table>
            </table>
            <a href="category.php" style="color: #fff;"><button type="button" class="btn" style="margin-top: 10px" >Back</button></a>
            <button type="submit" class="btn   pull-right" name="upd" style="margin-top: 10px" >Update</button>
            <?php }?>
        </form>
        </div>
        </div>
        </div>
  <?php include "include/footer.php" ;?>
  <?php include "include/right_sidebar.php" ;?>  
</div>
<?php include "include/footer_script.php" ;?>
<script src="jscolor.js"></script>
</body>
</html>
