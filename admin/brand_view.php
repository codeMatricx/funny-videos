<?php

include "database.php";
 $user_id = $_GET['uid'];
?>
<?php require('include/head.php'); ?>


<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  
  <?php include "include/header.php";?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include "include/left_sidebar.php";?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Brand Details
        
      </h1>
      
    </section>

    <!-- Main content -->

    <section class="content">
      <!-- Info boxes -->

     <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">

<?php 
                                    $sql = "SELECT * from brand WHERE id=$user_id";
                                    $result = $conn->query($sql);
                                    if ($result->num_rows>0)
                                    {
                                        $serial=1;
                                        
                                        if($brand = $result->fetch_assoc())
                                        {
                                            
                                    ?>

              <table  class="table table-bordered table-striped">
                
                <thead>
                <tr>
        <th>Brand Name</th>
        <td><?php  echo $brand['name'];?></td>
        
      </tr>
   
                </thead>
                <tbody>
                <tr>
        <th>Image</th>
        <td><img src="towfeerapi/uploads/<?php echo $brand['image']; ?>" class="img-responsive" width="100px" height="100px"></td>
        
      </tr>
           <tr>
        <th>Established In</th>
        <td><?php  echo $brand['establish'];?></td>
        
      </tr>  
           
               
                </tbody>


              </table>

<?php
                                        $serial++;
                                         } } ?>
              

             <a href="brand.php" style="color: #fff;"> <button type="button" class="btn  " style="margin-top: 10px">Back</button></a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
      
      
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <?php include "include/footer.php" ;?>
  
  
  <!-- Control Sidebar -->
  <?php include "include/right_sidebar.php" ;?>
  
</div>
<!-- ./wrapper -->

<?php include "include/footer_script.php" ;?>




</body>
</html>
