 <?php 
 include "database.php";
$id=$_GET['category_id'];
?>
<?php require('include/head.php'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php include "include/header.php";?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php include "include/left_sidebar.php";?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Categories Details
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
     <div class="box">     
            <!-- /.box-header -->
            <div class="box-body">
              <table  class="table table-bordered table-striped">
                         <?php 
                            $sql = "SELECT * from category WHERE id='$id'";
                            $result = $conn->query($sql);
                            if ($result->num_rows>0)
                            {
                            $category_view=$result->fetch_assoc();
                            ?>
                <tbody>
                 <tr>
                    <th>Categories</th>
                    <td><?php echo $category_view['category_name'];  ?></td> 
                </tr>
                </tbody>
                 <?php } ?>
              </table>
              <a href="add_categories.php" style="color: #fff;"><button type="button" class="btn  " style="margin-top: 10px">Back</button></a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include "include/footer.php" ;?>
  <!-- Control Sidebar -->
  <?php include "include/right_sidebar.php" ;?> 
</div>
<?php include "include/footer_script.php" ;?>
</body>
</html>
