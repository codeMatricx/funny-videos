<?php

include "database.php";


?>
<?php require('include/head.php'); ?>


<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  
  <?php include "include/header.php";?>
 <?php
 if(isset($_POST["sub"]))
{
$target_dir = "towfeerapi/uploads/";
$image=$_FILES["image"];
$target_file = $target_dir . basename($_FILES["image"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);


$name=$_POST["name"];

$establish=$_POST["establish"];
$status = 1;
$image="";
   if(empty($_FILES["image"]["tmp_name"]))
  {
    echo "Please choose File!";
    $uploadOk = 0;
  }
  else
  {
     if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
      && $imageFileType != "gif" ) 
   {
      echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
      $uploadOk = 0;
     }
   
    $check = getimagesize($_FILES["image"]["tmp_name"]);
    if($check != true) 
  {
        echo "File is not an image.";
        $uploadOk = 0;
    }
    else
  { 
   
  
     if ($_FILES["image"]["size"] > 500000)
   {
      echo "Sorry, your file is too large.";
      $uploadOk = 0;
     }
  }
  }
  
  if($uploadOk == 1)
  {
  if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file))
  {
     $image =  $_FILES["image"]["name"];
       echo "The file " . "&nbsp" . "<b>" . basename( $_FILES["image"]["name"]). "</b>" . "&nbsp" . " has been uploaded.";
    }
    }
$uid = $_SESSION['usr_id'];
$sql = "INSERT INTO brand (name, image, establish,user_id)
VALUES ('$name','$image','$establish','$uid')";



if ($conn->query($sql) === TRUE) {
    echo "New record created successfully";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}



}

if (isset($_POST['deleteProduct']))
{
    $id = ($_POST['id']);
    
    $sql = "DELETE FROM brand WHERE id= $id";
    if ($conn->query($sql) === TRUE)
    {
        //unlink($image_dir.$deleteimage);
        $responseMessage =  "User Remove successfully";
    }
    else
    {
        $responseMessage =  "Connection failed: " . $conn->error;
    }
}
?> 
  <!-- Left side column. contains the logo and sidebar -->
  <?php include "include/left_sidebar.php";?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Brand Details
        
      </h1>
      <ol class="breadcrumb">
        <li><button type="button" class="btn btn-block " style="margin-top: -5px;" onclick="div_show('addNews')" id="">Add Brand
        </button></li>
      </ol>
      
    </section>

    <!-- Main content -->

   <section class="content">
      <!-- Info boxes -->

     <div class="box">
            <div class="box-header ">
              <h3 class="box-title">Brand Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive table-scroll-y">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Brand Name</th>
                  <th>Image</th>
                  <th>Established In</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
               
               <?php 
                                    $sql = "SELECT * from brand WHERE user_id = '$uid'";
                                    $result = $conn->query($sql);
                                    if ($result->num_rows>0)
                                    {
                                        $serial=1;
                                        
                                        while($brand = $result->fetch_assoc())
                                        {
                                            
                                    ?>
                <tr id="<?php  echo $brand['id'];?>">
                  <td><?php echo $serial; ?></td>
                  <td><?php  echo $brand['name'];?></td>
                  <td><img src="towfeerapi/uploads/<?php echo $brand['image']; ?>" class="img-responsive" width="30px" height="30px"></td>
                  <td><?php  echo $brand['establish'];?></td>
                  <td><a href="brand_edit.php?uid=<?php echo $brand['id'];?>"  style="cursor: pointer;">
                     <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a>/
                     <a href="brand_view.php?uid=<?php echo $brand['id'];?>"  style="cursor: pointer;"><i class="fa fa-eye" aria-hidden="true"></i></a>/
                    <a class="<?php echo $brand['id'];?>" onclick="div_show('deleteProduct',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                </tr>

 <?php
                                        $serial++;
                                         } } ?>
                
                </tbody>
               
              </table>
            </div>
            <!-- /.box-body -->
          </div>  
          <!-- /.box -->
      
      
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <?php include "include/footer.php" ;?>
  
  
  <!-- Control Sidebar -->
  <?php include "include/right_sidebar.php" ;?>
  
</div>
<!-- ./wrapper -->

 <div id="addNews">
                    

  <div class="flappy-dialog">

  <h2>Add Brand</h2>
  <img id="close" src="assets/img/close.png" onclick="div_hide('addNews')" style="float: right; margin-top: -63px; padding-right: 6px;">
  <form  id="form" method="post" name="form" enctype="multipart/form-data"> 
  <input id="name" name="name" placeholder="Brand Name" type="text" required>
   <label for="newimage" class="btn text-muted text-center " style="width:82%;margin-top: 2%;">Choose Image</label>
   <input id="newimage" type="file" name="image" style="display:none;" required="required">
  <input id="establish" name="establish" placeholder="Established in (YYYY)" type="text" maxlength="4"; required>
    
  <div class="flappy-dialog-buttons" style="margin-top: 5px;">
    <div class="left-flap"></div>
    <input type="submit" name="sub" value="ADD">
    <div class="right-flap"></div>
  </div>
</form>
</div>
                        
                    </div>

    <div id="deleteProduct">
                    <!-- Popup Div Starts Here -->
                    <div id="popupDelete" class="popup">
                        <!-- Contact Us Form -->
                        <img id="close" src="assets/img/close.png" onclick="div_hide('deleteProduct')">
                       <form  id="form" method="post" name="form" enctype="multipart/form-data"> 
                            <hr>
                            <h2>Are You Sure??</h2>
                            <input type="submit" name="deleteProduct" value="OK">
                            <input type="hidden" name="id" id="deleteId">
                        </form>
                    </div>
 </div>                

<?php include "include/footer_script.php" ;?>




</body>
</html>
