<!-- jQuery 3.1.1 -->
<script src="plugins/jQuery/jquery-3.1.1.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="js/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="js/raphael-min.js"></script>
<script src="plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="js/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src="assets/js/main.js"></script>
<script>
$(function() {
    $('#category').change(function() {
          $('.subcategory').hide();
          var v =  $('#category option:selected').val();
          $('#subcategory_' + v).show();
    });
});
</script>
<!-- jQuery 3.1.1 -->

<script src="plugins/datatables/jquery.dataTables.min.js"></script>

<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="js/multiple_selection.js"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>
<script type="text/javascript">
  $(document).on('click', 'a[href^="#"]', function(e) {
    // target element id
    var id = $(this).attr('href');

    // target element
    var $id = $(id);
    if ($id.length === 0) {
        return;
    }

    // prevent standard hash navigation (avoid blinking in IE)
    e.preventDefault();

    // top position relative to the document
    var pos = $id.offset().top;

    // animated top scrolling
    $('body, html').animate({scrollTop: pos});
});
</script>
<script>
$(document).ready(function() 
  {
    $('#sus_block').on('change', function() 
    {
      var isChecked = $(this).is(':checked');
      if(isChecked) 
      {
        var user_id_data = '<?php echo $uid;?>';
        var user_status = 1;
        $.ajax(
        {
          type:'POST',
          datatype:'json',
          url : 'change_user_status.php',
          data : {user_id:user_id_data,status:user_status},
            success : function($data)
            {
              console.log($data);
            }
        });
      } 
      else 
      {
        var user_id_data = '<?php echo $uid;?>';
        var user_status = 0;
        $.ajax(
        {
          type:'POST',
          datatype:'json',
          url : 'change_user_status.php',
          data : {user_id:user_id_data,status:user_status},
            success : function($data)
            {
              console.log($data);
            }
        }); 
      }

      console.log('Selected data: ' + selectedData);

    });
});
</script>
<script type="text/javascript">
  function check_uncheck_checkbox(isChecked) {
  if(isChecked) 
  {
    $('input[name="action"]').each(function() 
    { 
      this.checked = true; 
    });
  } else {
    $('input[name="action"]').each(function() {
      this.checked = false;
    });
  }
}
</script>
<script type="text/javascript">
    $(function() {
        // this will get the full URL at the address bar
        var url = window.location.href;

        // passes on every "a" tag
        $(".sidebar a").each(function() {
            // checks if its the same on the address bar
            if (url == (this.href)) {
                $(this).closest("li").addClass("active");
                //for making parent of submenu active
               $(this).closest("li").parent().parent().addClass("active");
            }
        });
    });        
</script>

<!-- user status script start-->
<script type="text/javascript">
  function deactivate()
  {
    $("input:checkbox[name=action]:checked").each(function () 
    {
            //console.log($(this).val());
         var user_status = 0;
         var user_id_data = $(this).val();
         var methodName = "deactivateUsers";
          $.ajax(
            {
              type:'POST',
              datatype:'json',
              url : 'user_status.php',
              data : {user_id:user_id_data,status:user_status,mtdnme:methodName},
              success : function(data)
              {
              var obj = $.parseJSON(data); 
            }
          });
    });
    alert("User DeActivate Successfully");
    location.reload();
  }

  function activate()
  {
    //alert("call");
    $("input:checkbox[name=action]:checked").each(function () 
    {
          console.log($(this).val());
         var user_status = 1;
         var user_id_data = $(this).val();
         var methodName = "activateUsers";
          $.ajax(
            {
              type:'POST',
              datatype:'json',
              url : 'user_status.php',
              data : {user_id:user_id_data,status:user_status,mtdnme:methodName},
              success : function(data)
              {
                var obj = $.parseJSON(data);
              }
          });
    });
    alert("User Activate Successfully");
    location.reload();
  }

  //Delete user
   function deleteUser()
   {
     $("input:checkbox[name=action]:checked").each(function () 
     {
          // console.log($(this).val());
          //var user_status = 1;
          var user_id_data = $(this).val();
           $.ajax(
             {
               type:'POST',
               datatype:'json',
               url : 'user_status.php',
               data : {user_delete_id:user_id_data},
             success : function(data)
              {
                 var obj = $.parseJSON(data);
            }
          });
     });
     alert("Select Ok For Delete Data");
     location.reload();
   }

// Licence
function licenceApproved()
  {
    $("input:checkbox[name=action]:checked").each(function () 
    {
          console.log($(this).val());
         var licence_status = 1;
         var methodName = "livenceapp";
         var user_id_data = $(this).val();
          $.ajax(
            {
              type:'POST',
              datatype:'json',
              url : 'user_status.php',
              data : {user_id:user_id_data,licence:licence_status,mtdnme:methodName},
              success : function(data)
              {
                var obj = $.parseJSON(data);
                  // if(obj.code == 404)
                  // {
                  //    alert(obj.message);
                  // }
              }
          });
    });
    //alert("Licence Approved Successfully");
    location.reload();
  }

// CrediCard
function creditCardApproved()
  {
    $("input:checkbox[name=action]:checked").each(function () 
    {
          console.log($(this).val());
         var creditCard_status = 1;
         var user_id_data = $(this).val();
         var methodName = "creditapp";
          $.ajax(
            {
              type:'POST',
              datatype:'json',
              url : 'user_status.php',
              data : {user_id:user_id_data,creditCard:creditCard_status,mtdnme:methodName},
              success : function(data)
              {
                var obj = $.parseJSON(data);
              }
          });
    });
    //alert("CreditCard Approved Successfully");
    location.reload();
  }



</script>
<!-- user status script End -->

<!-- post status -->
<script type="text/javascript">
  function postDeactivate()
  {
    $("input:checkbox[name=action]:checked").each(function () 
    {
         var post_status = 0;
         var post_id_data = $(this).val();
          $.ajax(
            {
              type:'POST',
              datatype:'json',
              url : 'post_status.php',
              data : {post_id:post_id_data,status:post_status},
              success : function(data)
              {
              var obj = $.parseJSON(data);
              
            }
          });
    });
    alert("Post DeActivate Successfully");
    location.reload();
  }

  function postActivate()
  {
    $("input:checkbox[name=action]:checked").each(function () 
    {
          console.log($(this).val());
         var post_status = 1;
         var post_id_data = $(this).val();
          $.ajax(
            {
              type:'POST',
              datatype:'json',
              url : 'post_status.php',
              data : {post_id:post_id_data,status:post_status},
              success : function(data)
              {
                var obj = $.parseJSON(data);
              }
          });
    });
    alert("Post Activate Successfully");
    location.reload();
  }

  //Delete Post
  function deletePost()
   {
     $("input:checkbox[name=action]:checked").each(function () 
     {
          var post_id_data = $(this).val();
           $.ajax(
             {
               type:'POST',
               datatype:'json',
               url : 'post_status.php',
               data : {delet_post_id:post_id_data},
             success : function(data)
              {
                 var obj = $.parseJSON(data);
            }
          });
     });
     alert("Select Ok For Delete Data");
     location.reload();
   }
</script>
<!-- post status end -->

<!-- Promotion status -->
<script type="text/javascript">
  function promotionDeactivate()
  {
    $("input:checkbox[name=action]:checked").each(function () 
    {
         var promotion_status = 0;
         var promotion_id_data = $(this).val();
          $.ajax(
            {
              type:'POST',
              datatype:'json',
              url : 'promotion_status.php',
              data : {promotion_id:promotion_id_data,status:promotion_status},
              success : function(data)
              {
              var obj = $.parseJSON(data);
              
            }
          });
    });
    alert("Promotion DeActivate Successfully");
    location.reload();
  }

  function promotionActivate()
  {
    $("input:checkbox[name=action]:checked").each(function () 
    {
          console.log($(this).val());
         var promotion_status = 1;
         var promotion_id_data = $(this).val();
          $.ajax(
            {
              type:'POST',
              datatype:'json',
              url : 'promotion_status.php',
              data : {promotion_id:promotion_id_data,status:promotion_status},
              success : function(data)
              {
                var obj = $.parseJSON(data);
              }
          });
    });
    alert("Promotion Activate Successfully");
    location.reload();
  }

  //Delete Promotion
  function deleteCategories()
   {
     $("input:checkbox[name=action]:checked").each(function () 
     {
          var category_id_data = $(this).val();
           $.ajax(
             {
               type:'POST',
               datatype:'json',
               url : 'category_status.php',
               data : {delet_category_id:category_id_data},
             success : function(data)
              {
                 var obj = $.parseJSON(data);
            }
          });
     });
     alert("Select Ok For Delete Data");
     location.reload();
   }
</script>
<!-- Promotion status end -->

<!-- Category status -->
<script type="text/javascript">
  function categoryDeactivate()
  {
    $("input:checkbox[name=action]:checked").each(function () 
    {
         var category_status = 0;
         var category_id_data = $(this).val();
          $.ajax(
            {
              type:'POST',
              datatype:'json',
              url : 'category_status.php',
              data : {category_id:category_id_data,status:category_status},
              success : function(data)
              {
              var obj = $.parseJSON(data);
              
            }
          });
    });
    alert("Category DeActivate Successfully");
    location.reload();
  }

  function categoryActivate()
  {
    $("input:checkbox[name=action]:checked").each(function () 
    {
          console.log($(this).val());
         var category_status = 1;
         var category_id_data = $(this).val();
          $.ajax(
            {
              type:'POST',
              datatype:'json',
              url : 'category_status.php',
              data : {category_id:category_id_data,status:category_status},
              success : function(data)
              {
                var obj = $.parseJSON(data);
              }
          });
    });
    alert("Category Activate Successfully");
    location.reload();
  }

 function deleteCategory()
   {
     $("input:checkbox[name=action]:checked").each(function () 
     {
          var user_id_data = $(this).val();
           $.ajax(
             {
               type:'POST',
               datatype:'json',
               url : 'category_status.php',
               data : {cat_delete_id:user_id_data},
             success : function(data)
              {
                 var obj = $.parseJSON(data);
            }
          });
     });
     alert("Select Ok For Delete Data");
     location.reload();
   }
</script>
<!-- Category status end -->

<!-- subCategory status -->
<script type="text/javascript">
  function subCategoryDeactivate()
  {
    $("input:checkbox[name=action]:checked").each(function () 
    {
         var sub_category_status = 0;
         var sub_category_id_data = $(this).val();
          $.ajax(
            {
              type:'POST',
              datatype:'json',
              url : 'sub_category_status.php',
              data : {sub_category_id:sub_category_id_data,status:sub_category_status},
              success : function(data)
              {
              var obj = $.parseJSON(data);
              
            }
          });
    });
    alert("subCategory DeActivate Successfully");
    location.reload();
  }

  function subCategoryActivate()
  {
    $("input:checkbox[name=action]:checked").each(function () 
    {
          console.log($(this).val());
         var sub_category_status = 1;
         var sub_category_id_data = $(this).val();
          $.ajax(
            {
              type:'POST',
              datatype:'json',
              url : 'sub_category_status.php',
              data : {sub_category_id:sub_category_id_data,status:sub_category_status},
              success : function(data)
              {
                var obj = $.parseJSON(data);
              }
          });
    });
    alert("subCategory Activate Successfully");
    location.reload();
  }

  //Delete Subcategory
  function deleteSubCategory()
   {
     $("input:checkbox[name=action]:checked").each(function () 
     {
          var user_id_data = $(this).val();
           $.ajax(
             {
               type:'POST',
               datatype:'json',
               url : 'sub_category_status.php',
               data : {delet_sub_cat_id:user_id_data},
             success : function(data)
              {
                 var obj = $.parseJSON(data);
            }
          });
     });
     alert("Select Ok For Delete Data");
     location.reload();
   }
</script>
<!-- subCategory status end -->

<!-- Commission status -->
<script type="text/javascript">
  function commissionDeactivate()
  {
    $("input:checkbox[name=action]:checked").each(function () 
    {
         var commission_status = 0;
         var commission_id_data = $(this).val();
          $.ajax(
            {
              type:'POST',
              datatype:'json',
              url : 'commission_status.php',
              data : {commission_id:commission_id_data,status:commission_status},
              success : function(data)
              {
              var obj = $.parseJSON(data);
              
            }
          });
    });
    alert("Commission DeActivate Successfully");
    location.reload();
  }

  function commissionActivate()
  {
    $("input:checkbox[name=action]:checked").each(function () 
    {
          console.log($(this).val());
         var commission_status = 1;
         var commission_id_data = $(this).val();
          $.ajax(
            {
              type:'POST',
              datatype:'json',
              url : 'commission_status.php',
              data : {commission_id:commission_id_data,status:commission_status},
              success : function(data)
              {
                var obj = $.parseJSON(data);
              }
          });
    });
    alert("Commission Activate Successfully");
    location.reload();
  }

   //Delete Commission
  function deleteCommission()
   {
     $("input:checkbox[name=action]:checked").each(function () 
     {
          var user_id_data = $(this).val();
           $.ajax(
             {
               type:'POST',
               datatype:'json',
               url : 'commission_status.php',
               data : {delet_commission_id:user_id_data},
             success : function(data)
              {
                 var obj = $.parseJSON(data);
            }
          });
     });
     alert("Select Ok For Delete Data");
     location.reload();
   }
</script>
<!-- Commission status end -->

<!-- //Delete Notification -->
<script type="text/javascript">
function deleteNotification()
   {
     $("input:checkbox[name=action]:checked").each(function () 
     {
          var notification_id_data = $(this).val();
           $.ajax(
             {
               type:'POST',
               datatype:'json',
               url : 'notification_status.php',
               data : {delet_notification_id:notification_id_data},
             success : function(data)
              {
                 var obj = $.parseJSON(data);
            }
          });
     });
     alert("Select Ok For Delete Data");
     location.reload();
   }
</script>