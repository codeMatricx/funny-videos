 <?php 
 include "database.php";
 //$user_id=$_SESSION['usr_id'];
if (isset($_POST['deleteProduct']))
{
    $id = ($_POST['id']);
    $sql = "DELETE FROM category WHERE id= '$id'";
    if ($conn->query($sql) === TRUE)
    {
       $responseMessage =  "Remove successful";
    }
    else
    {
       $responseMessage =  "Connection failed: " . $conn->error;
    }
}
?>

<?php 
if(isset($_POST["add"]))
{
       
      $category=$_POST["category"];
      
      $sql = "INSERT INTO category (category_name)
      VALUES ('$category')";
      if ($conn->query($sql) === TRUE) 
      {
          //echo "category insert Succesfull";
          header("location:add_categories.php");
      } 
      else
      {
          echo "Error: " . $sql . "<br>" . $conn->error;
      }
}
  ?>
<?php require('include/head.php'); ?>
<body onload="initialize()" class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php include "include/header.php";?> 
  <?php include "include/left_sidebar.php";?>
   

  <!-- Left side column. contains the logo and sidebar -->
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
          Categories Details 
          </h1>
          <ol class="breadcrumb">
          <li><button type="button" class="btn btn-block " style="margin-top: -5px;" onclick="div_show('addNews')" id="">Add Categories</button></li>
          </ol>
        </section>
<section class="content">
 <div class="box">
      <div class="box-header">
        <h3 class="box-title">Categories Table With Full Features</h3>
      </div>
       <div class="box-body table-responsive table-scroll-y">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>S.NO</th>
              <th>Categories Name</th>
              <th>Action</th>
              <th><input name="select-all" id="checkall" onClick="check_uncheck_checkbox(this.checked);" value="check_all" type="checkbox"></th>
            </tr>
            </thead>
            <tbody>
                    <?php 
                    $sql = "SELECT id,category_name FROM category";
                    $result = $conn->query($sql);
                    if ($result->num_rows>0)
                    {
                    $serial=0;
                    while($category = $result->fetch_assoc())
                    {
                    $serial++;
                    ?>
              <tr id="<?php  echo $category['id']; ?>">
                <td><?php echo $serial; ?></td>
                <td><?php echo $category['category_name']; ?></td>
                  <td><a href="add_categories_edit.php?category_id=<?php  echo $category['id'];?>"  style="cursor: pointer;">
                  <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a>/
                  <a href="add_categories_view.php?category_id=<?php  echo $category['id'];?>"  style="cursor: pointer;"><i class="fa fa-eye" aria-hidden="true"></i></a>
                </td>
                <td><input   type="checkbox" value="<?php echo $category['id'];?>" name="action" id="checkbox_promotion"></td>
              </tr> 
                   <?php 
                     } 
                    }
                     ?>
            </tbody>
              </table>
                <button class="btn btn-danger" onclick="deleteCategories()">Delete</button>
            </div>
            <!-- /.box-body -->
 </div>
</div>
  <!-- /.content-wrapper -->
  <?php include "include/footer.php" ;?>
  <?php include "include/right_sidebar.php" ;?>  
</div>
<div id="addNews">                    
<div class="flappy-dialog">
  <h2>Add Categories</h2>
    <img id="close" src="assets/img/close.png" onclick="div_hide('addNews')" style="float: right; margin-top: -63px; padding-right: 6px;">
                <form  id="form" method="post" name="form" enctype="multipart/form-data">
	                <input id="category" name="category" placeholder="Categories" type="text" required>
	              	<div class="flappy-dialog-buttons" style="margin-top: 5px;">
		              	<div class="left-flap"></div>
		              		<input type="submit" name="add" value="ADD">
		              	<div class="right-flap"></div>
	              	</div>
              </form>
    </div>
</div>

<div id="deleteProduct">
  <div id="popupDelete" class="popup">
  	<img id="close" src="assets/img/close.png" onclick="div_hide('deleteProduct')">
	  <form method="post">
		  <hr>
		  	<h2>Are You Sure??</h2>
			  <input type="submit" name="deleteProduct" value="OK">
			  <input type="hidden" name="id" id="deleteId">
	  </form>
  </div>
</div>
<?php include "include/footer_script.php" ;?>
<script src="js/script.js"> </script>
</body>
</html>
