<?php
include "database.php";
?>
<?php require('include/head.php'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper"> 
  <?php include "include/header.php";?>
  <?php include "include/left_sidebar.php";?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Funny Videos Administration control Panel   
      </h1>
    <section class="content">
      <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12">
          <a href="user.php">
            <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-user"></i></span>
            <div class="info-box-content">
                  <?php 
                  $sql = "SELECT count(id) AS cnt from users ";
                  $result = $conn->query($sql);
                  $numrow = $result->fetch_row();
                  $countUser = $numrow['0']; 
                  ?>
              <span class="info-box-text">Users</span>
              <span class="info-box-number"><?php echo $countUser; ?><small></small></span>
            </div>
          </div>
        </a>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <a href="add_categories.php">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-clipboard"></i></span>

            <div class="info-box-content">
              <?php 
                  $sql = "SELECT count(id) AS cnt from category ";
                  $result = $conn->query($sql);
                  $numrow = $result->fetch_row();
                  $countCategory = $numrow['0']; 
                  ?>
              <span class="info-box-text">Add Categories</span>
              <span class="info-box-number"><?php echo $countCategory; ?></span>
            </div>
          </div>
        </a>
        </div>
        <div class="clearfix visible-sm-block"></div>
        <!-- <div class="col-md-4 col-sm-6 col-xs-12">
          <a href="order.php">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-sort"></i></span>
            <div class="info-box-content">
              <?php 
                  $sql = "SELECT count(id) AS cnt from orders ";
                  $result = $conn->query($sql);
                  $numrow = $result->fetch_row();
                  $countOrder = $numrow['0']; 
                  ?>
              <span class="info-box-text">Order</span>
              <span class="info-box-number"><?php echo $countOrder; ?></span>
            </div>
          </div>
        </a>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <a href="category.php">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-list"></i></span>

            <div class="info-box-content">
              <?php 
                  $sql = "SELECT count(id) AS cnt from category ";
                  $result = $conn->query($sql);
                  $numrow = $result->fetch_row();
                  $countOCat = $numrow['0']; 
                  ?>
              <span class="info-box-text">Category</span>
              <span class="info-box-number"><?php echo $countOCat; ?></span>
            </div>
          </div>
        </a>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <a href="promotion_list.php">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-gift"></i></span>

            <div class="info-box-content">
              <?php 
                  $sql = "SELECT count(id) AS cnt from manage_coupon ";
                  $result = $conn->query($sql);
                  $numrow = $result->fetch_row();
                  $countPro = $numrow['0']; 
                  ?>
              <span class="info-box-text">Promotions</span>
              <span class="info-box-number"><?php echo $countPro; ?></span>
            </div>
          </div>
        </a>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <a href="sub_category.php">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-th-list"></i></span>

            <div class="info-box-content">
               <?php 
                  $sql = "SELECT count(id) AS cnt from sub_category ";
                  $result = $conn->query($sql);
                  $numrow = $result->fetch_row();
                  $countSubCat = $numrow['0']; 
                  ?>
              <span class="info-box-text">Sub Category </span>
              <span class="info-box-number"><?php echo $countSubCat; ?></span>
            </div>
          </div>
        </a>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <a href="notification.php">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-comments"></i></span>

            <div class="info-box-content">
               <?php 
                  $sql = "SELECT count(id) AS cnt from notifications ";
                  $result = $conn->query($sql);
                  $numrow = $result->fetch_row();
                  $countnote = $numrow['0']; 
                  ?>
              <span class="info-box-text">Notifications</span>
              <span class="info-box-number"><?php echo $countnote; ?></span>
            </div>
           
          </div>
        </a>
          
        </div> -->

        
  
      </div>
  </section>
  </div>
  <?php include "include/footer.php" ;?>
  <?php include "include/right_sidebar.php" ;?>  
</div>
<?php include "include/footer_script.php" ;?>
</body>
</html>
