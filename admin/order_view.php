<?php 
include "database.php";
$id=$_GET['order_id'];
?>
<?php require('include/head.php'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php include "include/header.php";?>
<?php include "include/left_sidebar.php";?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
    Order View    
    </h1>   
  </section>
<section class="content">
<div class="box">
    <div class="box-body">
    	<?php 
                  $sql = "SELECT o.total_amount,m.coupon_code,u.name AS user_name,u.email,u.id AS user_id,o.order_date,o.id AS order_id,o.order_date,o.payment_mode,sc.sub_category_name AS product_name 
		                  from orders AS o 
		                  INNER JOIN users AS u ON o.user_id=u.id 
		                  INNER JOIN manage_coupon AS m ON o.manage_coupon_id=m.id 
		                  INNER JOIN sub_category AS sc ON o.sub_category_id=sc.id 
		                  WHERE o.id='$id'";
                  $result = $conn->query($sql);
                  if ($result->num_rows>0)
                  {
                 $order = $result->fetch_assoc();
                  ?> 
      <h4><b>Order Details</b></h4>
      <table  class="table table-bordered table-striped">
          <tbody>
                   
                <tr>
                <th>Order Date:</th>
                <td><?php  echo $order['order_date'];?></td> 
                </tr>
                <tr>
                <th>User ID:</th>
                <td><?php  echo $order['user_id'];?></td> 
                </tr>
                <tr>
                <th>  Order ID:</th>
                <td><?php  echo $order['order_id'];?></td> 
                </tr>
                <tr>
                <th>Email:</th>
                <td><?php  echo $order['email'];?></td> 
                </tr>
                
          </tbody>      
          </table>
                 
              <h4><b>Delivery Information</b></h4>
          <table  class="table table-bordered table-striped">
                    
                <tbody>
                  <tr>
                    <th>Name:</th>
                    <td><?php  echo $order['user_name'];?></td> 
                </tr>
                <tr>
                    <th>Email:</th>
                    <td><?php  echo $order['email'];?></td> 
                </tr>
                </tbody>
                
            </table>
              <h4><b>Discount Information</b></h4>
            <table  class="table table-bordered table-striped">
                <tbody>
                  <tr>
                    <th>Coupoun Code Used:</th>
                    <td><?php  echo $order['coupon_code'];?></td> 
                </tr>
                </tbody>
            </table>
              <h4><b>Payment Information</b></h4>
            <table  class="table table-bordered table-striped">
                <tbody>
                  <tr>
                    <th>Payment Method:</th>
                    <td><?php  echo $order['payment_mode'];?></td> 
                </tr>
                </tbody>
            </table>
              <h4><b>Ordered Products</b></h4>
            <table  class="table table-bordered table-striped">
                <tbody>
                  <tr>
                    <th>Product Purchased</th>
                    <!-- <th>Qty</th>
                    <th>Unit Price(Rs)</th> -->
                    <th>Price (Rs)</th>
                </tr>
                <tr>
                    <td><?php  echo $order['product_name'];?></td>
                    <!-- <td>20</td>
                    <td>  193.50</td> -->
                    <td><?php  echo $order['total_amount'];?></td>
                </tr>
                </tbody>
            </table>
<a href="order.php" style="color: #fff;"><button type="button" class="btn  " style="margin-top: 10px">Back</button></a>
<?php } ?> 
</div>
</div>
</div>
<?php include "include/footer.php" ;?>
<?php include "include/right_sidebar.php" ;?>
</div>
<div id="deleteProduct">
  <div id="popupDelete" class="popup">
  <img id="close" src="assets/img/close.png" onclick="div_hide('deleteProduct')">
  <form method="post">
  <hr>
  <h2>Are You Sure??</h2>
  <input type="submit" name="deleteProduct" value="OK">
  <input type="hidden" name="id" id="deleteId">
  </form>
  </div>
</div>
<?php include "include/footer_script.php" ;?>
</body>
</html>
