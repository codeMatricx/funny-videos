 <?php 
 include "database.php";
?>
<?php
$sub_cat_id=$_GET['sub_cat_id'];
if (isset($_POST["upd"])) 
{
        if(empty($_FILES['image']['name']))
        {
        $image = $_POST['image_first'];
        }
        else
        {
        $target_dir = "assets/img/uploads/category/";
        $target_file = $target_dir . basename($_FILES["image"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
        $image =  $_FILES["image"]["name"];
        $uploadOk = 1;
        }

$category_id=$_POST["category_id"];
$sub_category_name  =$_POST["sub_category_name"];   
$subColor  =$_POST["subColor"];
    $sql = "UPDATE sub_category SET category_id='$category_id',sub_category_name='$sub_category_name',image='$image',color_code='$subColor' WHERE id='$sub_cat_id'";
         if ($conn->query($sql) === TRUE)
          {
            header("location:sub_category.php");
          }
            else
            {
              $responseMessage =  "Connection failed: " . $conn->connect_error;
            }
}

?>
<?php require('include/head.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php include "include/header.php";?>
 
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include "include/left_sidebar.php";?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Sub Category Edit/Update
        
      </h1>
    </section>

    <!-- Main content -->

    <section class="content">
      <!-- Info boxes -->

     <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body table-responsive table-scroll-y">
              <form method="post" enctype="multipart/form-data">
                <tbody>
                      <?php 
                      $sql = "SELECT * from sub_category WHERE id='$sub_cat_id'";
                      $result = $conn->query($sql);
                      if ($result->num_rows>0)
                      {
                        $sub_cat=$result->fetch_assoc();
                      $sub_category = $sub_cat['sub_category_name'];
                       }
                      ?>
              <table id="example1" class="table table-bordered table-striped">
                <table  class="table table-bordered table-striped">               
                <tbody>
                      <tr>
                      &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button class="jscolor
                      {valueElement:'valueInput', styleElement:'styleInput'}">
                      Choose Color
                      </button>
                      <p>
                      <td>Value: <input id="valueInput" name="subColor" value="<?php echo $sub_cat['color_code']; ?>"><br></td>
                      <td>Style: <input id="styleInput"></td>
                      </tr>

                <tr>
            
                  <td>
                  <select name="category_id" style="margin-left:0px;width: 34%;padding: 5px;">
                          <?php 
                          $sql = "SELECT * from category";
                          $result = $conn->query($sql);
                          if ($result->num_rows>0)
                          {
                          $serial=0;
                          while($category = $result->fetch_assoc())
                            {
                            ?>
                                <option value="<?php echo $category['id'];?>" selected disable><?php echo $category['category_name'];?></option>   
                            <?php 
                            } 
                          }
                          ?>       
                  </select> 
                </td>
                </tr>
                <tr>
                
                  <td><input type="text" name="sub_category_name" value="<?php echo $sub_category;?>" placeholder="Sub Category Name" style="width: 34%;" /></td>
                </tr>
                <tr>
               <th>Sub Category Icon</th>
               <td><img src="assets/img/uploads/category/<?php echo $sub_cat['image']; ?>" class="img-responsive" style="width:30px; height:30px" ></td> 
              </tr>
              <tr>
              <th>Change Icon</th>
                   <td><label for="newimage" class="btn text-muted text-center btn-success" style="width:20%;margin-top: -4px;padding: 12px;">Icon</label>
                   <input id="newimage" type="file" name="image" style="display: none;">
                   <input type = "hidden" name = "image_first" id = "image_first" value = "<?php  echo $sub_cat['image'];?>">
               </td>
               </tr>
                </tbody>
              <?php 
              
              ?>
              </table>
              </table>

             

              <a href="sub_category.php" style="color: #fff;"><button type="button" class="btn" style="margin-top: 10px" >Back</button></a>
              <button type="submit" class="btn   pull-right" name="upd" style="margin-top: 10px" >Update</button>
              <?php
               //}
              ?>
            </form>
            </div>
           
          </div> 
  </div>
  <!-- /.content-wrapper -->
  <?php include "include/footer.php" ;?>
 <?php include "include/right_sidebar.php" ;?>
<?php include "include/footer_script.php" ;?>
<script src="jscolor.js"></script>
</body>
</html>
