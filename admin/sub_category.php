 <?php 
 include "database.php";
?>
<?php 
if(isset($_POST["sub"]))
{

$target_dir = "assets/img/uploads/category/";
$image = $_FILES['image']["name"];
$target_file = $target_dir . basename($_FILES['image']["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

$category_id=$_POST["category_id"];
$sub_category_name  =$_POST["sub_category_name"];
$status = 1;
$image="";

            if(empty($_FILES["image"]["tmp_name"]))
            {
            echo "Please choose File!";
            $uploadOk = 0;
            }
            else
            {
              if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
              && $imageFileType != "gif" ) 
              {
              echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
              $uploadOk = 0;
              }
              $check = getimagesize($_FILES["image"]["tmp_name"]);
              if($check != true) 
              {
              echo "File is not an image.";
              $uploadOk = 0;
              }
              else
              { 
                if ($_FILES["image"]["size"] > 500000)
                {
                echo "Sorry, your file is too large.";
                $uploadOk = 0;
                }
                else
                  {
                    $uploadOk = 1;
                  }
              }
            }
            if($uploadOk == 1)
            {
              if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file))
              {
              $image =  $_FILES["image"]["name"];
              }
            }

      $sql = "INSERT INTO sub_category (category_id,sub_category_name,image)
      VALUES ('$category_id','$sub_category_name','$image')";
      if ($conn->query($sql) === TRUE) 
      {
      echo "New record created successfully";
      } 
      else 
      {
      echo "Error: " . $sql . "<br>" . $conn->error;
      }
}

if (isset($_POST['deleteProduct']))
{
    $id = $_POST['id'];
    $sql = "DELETE FROM sub_category WHERE id= $id";
    if ($conn->query($sql) === TRUE)
    {
        $responseMessage =  "User Remove successfully";
    }
    else
    {
        $responseMessage =  "Connection failed: " . $conn->error;
    }
}
?>
<?php require('include/head.php'); ?>
<body onload="initialize()" class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php include "include/header.php";?>
<?php include "include/left_sidebar.php";?>
<div class="content-wrapper">
<section class="content-header">
<h1>
Sub Category Details   
</h1>
<ol class="breadcrumb">
<li><a href="addSubCategory.php"><button type="button" class="btn btn-block " style="margin-top: -5px;">Add Sub Category</button></a></li>
</ol>
</section>
<section class="content">
<div class="box">
<div class="box-header">
<h3 class="box-title">Sub Category Table With Full Features</h3>
</div>
            <!-- /.box-header -->
            <div class="box-body table-responsive table-scroll-y">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.NO</th>
                  <th>Category Name</th>
                  <th>Sub Category Name</th>
                  <th>Icon</th>
                  <th>Status </th>
                  <!-- <th>Activate/Deactivate</th> -->
                  <th>Action</th>
                  <th><input name="select-all" id="checkall" onClick="check_uncheck_checkbox(this.checked);" value="check_all" type="checkbox"></th>
                </tr>
                </thead>
                    <tbody>
                      <?php 
                      $sql = "SELECT s.sub_category_status,s.id,c.category_name,s.sub_category_name,s.image from sub_category AS s INNER JOIN category AS c ON s.category_id=c.id";
                      $result = $conn->query($sql);
                      if ($result->num_rows>0)
                      {
                      $serial=0;
                      while($sub_category = $result->fetch_assoc())
                      {
                      $serial++;
                      ?>
                    <tr id="<?php  echo $sub_category['id'];?>">
                    <td><?php echo $serial; ?></td>
                      <td><?php echo $sub_category['category_name'];?></td>
                      <td><?php echo $sub_category['sub_category_name'];?></td>
                      <td><img src="assets/img/uploads/category/<?php echo $sub_category['image']; ?>" class="img-responsive" style="width:30px; height:30px" ></td>

                        <?php 
                        if($sub_category['sub_category_status'] == 1)
                        {
                        ?>
                        <td><img src="assets/img/enable.gif" class="img-responsive" title="Activated"></td>
                        <?php
                        }
                        ?>
                        <?php
                        if($sub_category['sub_category_status'] == 0)
                        {
                        ?>
                        <td><img src="assets/img/disable.gif" class="img-responsive" title="Deactivated"></td>
                        <?php
                        }
                        ?>
                    <!-- <td><label class="switch">
                    <input type="checkbox" checked>
                    <span class="slider round"></span>
                    </label>
                    </td> -->
                    <td >
                    <a href="sub_category_edit.php?sub_cat_id=<?php echo $sub_category['id']; ?>" style="cursor: pointer;">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a><!-- / -->
                    <!-- <a href="sub_category_view.php?sub_cat_id=<?php echo $sub_category['id']; ?>"  style="cursor: pointer;"><i class="fa fa-eye" aria-hidden="true"></i></a>/ -->
                    <!-- <a class="<?php echo $sub_category['id'];?>" onclick="div_show('deleteProduct',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;"><i class="fa fa-trash-o" aria-hidden="true"></i></a> -->
                    </td>
                    <td><input   type="checkbox" value="<?php echo $sub_category['id'];?>" name="action" id="checkbox-1"></td>
                    </tr> 
                    <?php 
                    }
                    } 
                    ?>
                    </tbody>
              </table>
<button class="btn btn-success" onclick="subCategoryActivate()">Activate</button>
<button class="btn btn-danger" onclick="subCategoryDeactivate()">Deactivate</button>
<button class="btn btn-danger" onclick="deleteSubCategory()">Delete</button>
</div>
</div>
</div>
<?php include "include/footer.php" ;?>
<?php include "include/right_sidebar.php" ;?>
</div>
<div id="addNews">                  
        <div class="flappy-dialog">
        <h2>Add SubCategory</h2>
        <img id="close" src="assets/img/close.png" onclick="div_hide('addNews')" style="float: right; margin-top: -63px; padding-right: 6px;">
        <form  id="form" method="post" name="form" enctype="multipart/form-data">
                
        <select name="category_id" style="margin-left:0px;" required>
          <?php 
                $sql = "SELECT * from category";
                $result = $conn->query($sql);
                if ($result->num_rows>0)
                {
                $serial=0;
                while($category = $result->fetch_assoc())
                {
                ?>
        <option value="<?php echo $category['id'];?>" selected disable><?php echo $category['category_name'];?></option>   
        <?php 
        } }
        ?>       
        </select>
        

        <input id="name" name="sub_category_name" placeholder="Sub Category Name" type="text" required>
        <label for="newimage" class="btn text-muted text-center " style="width:82%;margin-top: 2%;">Choose Image</label>
        <input id="newimage" type="file" name="image" style="display:none">   
        <div class="flappy-dialog-buttons" style="margin-top: 5px;">
        <div class="left-flap"></div>
        <input type="submit" name="sub" value="ADD">
        <div class="right-flap"></div>
        </div>
        </form>
        </div>
</div>

<div id="deleteProduct">
<div id="popupDelete" class="popup">
<img id="close" src="assets/img/close.png" onclick="div_hide('deleteProduct')">
<form method="post">
<hr>
<h2>Are You Sure??</h2>
<input type="submit" name="deleteProduct" value="OK">
<input type="hidden" name="id" id="deleteId">
</form>
</div>
</div>

<?php include "include/footer_script.php" ;?>
<script src='http://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyAL1eAba9HoD7qszOJ-ggOvZvbq-TvXDys'></script>
<script src="js/script.js"> </script>
</body>
</html>
