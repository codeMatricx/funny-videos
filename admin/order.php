 <?php 
 include "database.php";
if (isset($_POST['deleteProduct']))
{
    $id = ($_POST['id']);
    $sql = "DELETE FROM branch WHERE id= $id";
    if ($conn->query($sql) === TRUE)
    {
        $responseMessage =  "User Remove successfully";
    }
    else
    {
        $responseMessage =  "Connection failed: " . $conn->error;
    }
}
?>
<?php require('include/head.php'); ?>
<body onload="initialize()" class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php include "include/header.php";?>
<?php include "include/left_sidebar.php";?>
<div class="content-wrapper">
        <section class="content-header">
        <h1>
        Order Details      
        </h1>
        </section>
<section class="content">
<div class="box">
<div class="box-header">
 <h3 class="box-title">Orders Table With Full Features</h3>
</div>
    <div class="box-body table-responsive table-scroll-y">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>S.NO</th>
              <th>Order Date</th>
              <th>Order ID</th>
              <th>User Email</th>
              <th>Total Amount</th>
              <th>Admin Commission</th>
              <th>Commission Amount</th>
              <th>Payment Method</th>
              <th>Payment Status</th>
              <th>Details</th>
            </tr>
          </thead>
          <tbody>
              <?php 
                $sql = "SELECT ac.commission_value,o.id,o.order_date,o.total_amount,o.admin_commission_id,o.payment_mode,ac.commission_value,u.email,ac.commission_type_id  FROM orders AS o INNER JOIN admin_commission AS ac ON 
                o.admin_commission_id=ac.id INNER JOIN users AS u ON o.user_id=u.id";
                  //print_r($sql);exit;
                $result = $conn->query($sql);
                if ($result->num_rows>0)
                {
                $serial=0;
                while($order = $result->fetch_assoc())
                {
                  
                  $amount=$order['total_amount'];
                  $commission=$order['commission_value'];

                    if($order['commission_type_id']==1)
                    {
                      $percent = $amount*($commission/100);
                      $total_commission= $amount-$percent;
                    }
                    if($order['commission_type_id']==2)
                    {
                      $total_commission=$amount-$commission;
                    }
                $serial++;1
                ?>
            <tr id="<?php  echo $order['id'];?>">
              <td><?php echo $serial; ?></td>
              <td><?php  echo $order['order_date'];?></td>
              <td><?php  echo $order['id'];?></td>
              <td><?php  echo $order['email'];?></td>
              <td><?php  echo $order['total_amount'];?></td>
                <?php
                if($order['commission_type_id']==1)
                {
                ?>
                <td><?php  echo $order['commission_value']."%";?></td>
                <?php 
                }
                ?>
                <?php
                if($order['commission_type_id']==2)
                {
                ?>
                <td><?php  echo $order['commission_value'];?></td>
                <?php 
                }
                ?>
              <td><?php  echo $total_commission ;?></td>
              <td><?php  echo $order['payment_mode'];?></td>
              <td align="center"><img src="assets/img/green.gif" title="successfully"></td>
              <td><a href="order_view.php?order_id=<?php  echo $order['id'];?>"  style="cursor: pointer;"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
            </tr>
            <?php } }?> 
          </tbody>             
        </table>
    </div>
</div>
  </div>
 <?php include "include/right_sidebar.php" ;?>
</div>
<?php include "include/footer_script.php" ;?>
<script src='http://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyAL1eAba9HoD7qszOJ-ggOvZvbq-TvXDys'></script>
<script src="js/script.js"> </script>
</body>
</html>
