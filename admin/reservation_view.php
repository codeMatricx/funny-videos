<?php

include "database.php";
$user_id = $_GET['uid'];
?>
<?php require('include/head.php'); ?>


<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  
  <?php include "include/header.php";?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include "include/left_sidebar.php";?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Reservation Details
        
      </h1>
      
    </section>

    <!-- Main content -->

    <section class="content">
      <!-- Info boxes -->

     <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">

<?php 
                                    $sql = "SELECT * from reservation WHERE id=$user_id";
                                    $result = $conn->query($sql);
                                    if ($result->num_rows>0)
                                    {
                                        $serial=1;
                                        
                                        if($reservation = $result->fetch_assoc())
                                        {
                                            
                                    ?>

              <table  class="table table-bordered table-striped">
                
                <thead>
                <tr>
        <th>Product Id</th>
        <td><?php  echo $reservation['product_id'];?></td>
        
      </tr>
   
                </thead>
                <tbody>
                <tr>
        <th>User Name</th>
        <td><?php  echo $reservation['name'];?></td>
        
      </tr>
      <tr>
        <th>Date</th>
        <td><?php  echo $reservation['date'];?></td>
        
      </tr>
      <tr>
        <th>Transaction No</th>
        <td><?php  echo $reservation['transaction'];?></td>
        
      </tr>  
      <tr>
        <th>Amount</th>
        <td><?php  echo $reservation['amount'];?> &#x24;</td>
        
      </tr>  
           
               
                </tbody>


              </table>

<?php
                                        $serial++;
                                         } } ?>




              <a href="reservation.php" style="color: #fff;"><button type="button" class="btn  " style="margin-top: 10px">Back</button></a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
      
      
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <?php include "include/footer.php" ;?>
  
  
  <!-- Control Sidebar -->
  <?php include "include/right_sidebar.php" ;?>
  
</div>
<!-- ./wrapper -->

<?php include "include/footer_script.php" ;?>




</body>
</html>
