 <?php 
 include "database.php";
 //$user_id=$_SESSION['usr_id'];
if (isset($_POST['deleteProduct']))
{
    $id = ($_POST['id']);
    $sql = "DELETE FROM post WHERE id= $id";
    if ($conn->query($sql) === TRUE)
    {
       $responseMessage =  "Remove successful";
    }
    else
    {
       $responseMessage =  "Connection failed: " . $conn->error;
    }
}
?>

<?php require('include/head.php'); ?>
<body onload="initialize()" class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php include "include/header.php";?> 
  <?php include "include/left_sidebar.php";?>
  <div class="content-wrapper">
	<section class="content-header">
	<h1>
	New Post Details 
	</h1>
	</section>
    <section class="content">
	<div class="box">
		<div class="box-header">
		<h3 class="box-title">Post Table With Full Features</h3>
		</div>
	  <div class="box-body table-responsive table-scroll-y">
		<table id="example1" class="table table-bordered table-striped">
		<thead>
			<tr>
				<th>S.NO</th>
				<th>User Name</th>
				<th>Title</th>
				<th>Category Name</th>
				<th>SubCategory Name</th>
				<th>Address</th>
				<th>Price</th>
				<th>Register Date</th>
				<th>Length of Rental</th>
				<th>Description</th>
				<th>Latitude</th>
				<th>Longitude</th>
				<!-- <th>Activate/Deactivate</th> -->
				<th>Status</th>
				<th>Action</th>
				<th><input name="select-all" id="checkall" onClick="check_uncheck_checkbox(this.checked);" value="check_all" type="checkbox"></th>
			</tr>
		</thead>
		<tbody>
				<?php 
				$sql = "SELECT p.status,p.lat,p.lng,p.id,u.name AS user_name,p.title,p.location,p.price,p.created_date_time,p.description,c.category_name,sc.sub_category_name,l.length_of_rental from post AS p INNER JOIN category AS c ON p.category=c.id INNER JOIN sub_category AS sc ON p.sub_category_id=sc.id INNER JOIN length_of_rental AS l ON p.length_of_rental=l.id INNER JOIN users AS u ON p.user_id=u.id Order By p.id";
				$result = $conn->query($sql);
				if ($result->num_rows>0)
				{
				$serial=0;
				while($post_list = $result->fetch_assoc())
				{
					$desc=$post_list['description'];
				$serial++;
				?>
			<tr id="<?php  echo $post_list['id'];?>">
				<td><?php echo $serial; ?></td>
				<td><?php  echo $post_list['user_name'];?></td>
				<td><?php  echo $post_list['title'];?></td>	
				<td><?php  echo $post_list['category_name'];?></td>
				<td><?php  echo $post_list['sub_category_name'];?></td>
				<td><?php  echo $post_list['location'];?></td>
				<td><?php  echo $post_list['price'];?></td>
				<td><?php  echo $post_list['created_date_time'];?></td>
				<td><?php  echo $post_list['length_of_rental'];?></td>
				<td><?php  echo substr ($desc, 0, 30);?></td>
				<td><?php  echo $post_list['lat'];?></td>
				<td><?php  echo $post_list['lng'];?></td>
				<!-- <td><label class="switch">
				<input type="checkbox" checked>
				<span class="slider round"></span>
				</label>
				</td> -->
					<?php 
					if($post_list['status'] == 1)
					{
					?>
					<td><img src="assets/img/enable.gif" class="img-responsive" title="Activated"></td>
					<?php
					}
					?>
					<?php
					if($post_list['status'] == 0)
					{
					?>
					<td><img src="assets/img/disable.gif" class="img-responsive" title="Deactivated"></td>
					<?php
					}
					?> 
				<td >
				<a href="post_list_view.php?post_id=<?php  echo $post_list['id'];?>"  style="cursor: pointer;"><i class="fa fa-eye" aria-hidden="true"></i></a><!-- /
				<a class="<?php echo $post_list['id'];?>" onclick="div_show('deleteProduct',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;"><i class="fa fa-trash-o" aria-hidden="true"></i></a> -->
				</td>
				<td><input   type="checkbox" value="<?php echo $post_list['id'];?>" name="action" id="checkbox_post"></td>
			</tr> 
			<?php } }?>
		</tbody>
		</table>
		<button class="btn btn-success" onclick="postActivate()">Activate</button>
		<button class="btn btn-danger" onclick="postDeactivate()">Deactivate</button>
		<button class="btn btn-danger" onclick="deletePost()">Delete</button>
	  </div>
    </div>
  </div>
  <?php include "include/footer.php" ;?>
  <?php include "include/right_sidebar.php" ;?>
</div>
<?php include "include/footer_script.php" ;?>
<script src='http://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyAL1eAba9HoD7qszOJ-ggOvZvbq-TvXDys'></script>
<script src="js/script.js"> </script>
</body>
</html>
