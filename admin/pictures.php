 <?php 
 include "database.php";
if (isset($_POST["deleteProduct"]))
{
    $id = ($_POST['id']);
    $sql = "DELETE FROM pictures
     WHERE id= $id";
    if ($conn->query($sql) === TRUE)
    {
        //unlink($image_dir.$deleteimage);
        $responseMessage =  "Picture Remove successfully";
    }
    else
    {
        $responseMessage =  "Connection failed: " . $conn->error;
    }
}
?>

<?php
if(isset($_POST["add"]))
{

$target_dir = "uploads/images/";

$target_file = $target_dir . basename($_FILES["image"]["name"]);

$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);


$categories=$_POST["categories"];
$name=$_POST["name"];
$description=$_POST["description"];
//print"<pre>";print_r($_POST);print"</pre>";exit;

$status = 1;
//image query
$image="";
   if(empty($_FILES["image"]["tmp_name"]))
  {
    echo "Please choose File!";
    $uploadOk = 0;
  }
  else
  {
     if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
      && $imageFileType != "gif" ) 
   {
      echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
      $uploadOk = 0;
     }
   
    $check = getimagesize($_FILES["image"]["tmp_name"]);
    if($check != true) 
  {
        echo "File is not an image.";
        $uploadOk = 0;
    }
  }
  
  if($uploadOk == 1)
  {
  if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file))
  {
     $image =  $_FILES["image"]["name"];
       //echo "The file " . "&nbsp" . "<b>" . basename( $_FILES["image"]["name"]). "</b>" . "&nbsp" . " has been uploaded.";
    }
    }
    $sql = "INSERT INTO pictures (item_name,type, image, description,categories)
    VALUES ('$name','3','$image','$description','$categories')";
    if ($conn->query($sql) === TRUE) 
    {  
      echo "New record created successfully";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
}
?>  
<?php require('include/head.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <?php include "include/header.php";?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php include "include/left_sidebar.php";?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <h1>
       Pictures Details 
      </h1>
        
      </h1>
      <ol class="breadcrumb">
        <li><button type="button" class="btn btn-block " style="margin-top: -5px;" onclick="div_show('addNews')" id="">Add PIcture</button></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
     <div class="box">
            <div class="box-header">
              <h3 class="box-title">Pictures Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive table-scroll-y">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Categories</th>
                  <th>Picture Name</th>
                  <th>Image</th>  
                  <th>Picture Description</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                    $sql = "SELECT * FROM pictures";
                    $result = $conn->query($sql);
                    if ($result->num_rows>0)
                    {
                    $serial=0;
                    while($pictures = $result->fetch_assoc())
                    {
                    $serial++;
                    ?>
                <tr id="<?php  echo $items['id']; ?>">
                  <td><?php echo $serial; ?></td>
                  <td>
                    <?php echo $pictures['categories']; ?>
                  </td>
                  <td><?php echo $pictures['item_name']; ?></td>
                  <td><img src="uploads/images/<?php echo $pictures['image']; ?>" class="img-responsive" width="30px" height="30px"></td>
                  <td><?php echo $pictures['description']; ?></td>
                  <td >
                    <a href="picture_edit.php?picture_id=<?php  echo $pictures['id'];?>"  style="cursor: pointer;">
                     <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a>/
                     <a href="picture_view.php?picture_id=<?php  echo $pictures['id'];?>"  style="cursor: pointer;"><i class="fa fa-eye" aria-hidden="true"></i></a>/
                    <a class="<?php echo $pictures['id'];?>" onclick="div_show('deleteProduct',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                </tr>
                <?php 
                     } 
                    }
                     ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <?php include "include/footer.php" ;?>
  <!-- Control Sidebar -->
  <?php include "include/right_sidebar.php" ;?>
</div>
<!-- ./wrapper -->
  <div id="addNews">
  <div class="flappy-dialog">
  <h2>Add Picture</h2>
  <img id="close" src="assets/img/close.png" onclick="div_hide('addNews')" style="float: right; margin-top: -63px; padding-right: 6px;">
  <form action=""  id="form" method="post" name="form" enctype="multipart/form-data">
  <select name="categories" id="cat" style="margin-left:0px;" required>
            <option value="0" selected disabled="disabled" >Select Category</option>
            <?php 
          $sql = "SELECT * from category";
          $result = $conn->query($sql);
          if ($result->num_rows>0)
          {
              $serial=1;
              
              while($category = $result->fetch_assoc())
              {
                  
          ?>
            <option value="<?php echo  $category['id'];?>"><?php  echo $category['category_name'];?></option>
            <?php
              $serial++;
             } } ?> 
  </select> 
  <input  type="text" name="name" placeholder="Name" required>
   <label for="newimage1" class="btn text-muted text-center " style="width:82%;margin-top: 2%;">Add Image</label>
  <input id="newimage1" type="file" name="image" style="display: none;" required>
  <textarea class="form-control" rows="5" id="comment" name="description" placeholder="Item Description" style="width:82%;margin-left:9%;margin-top: 2%; " required></textarea>   
  <div class="flappy-dialog-buttons" style="margin-top: 5px;">
    <div class="left-flap"></div>
    <input type="submit" name="add" value="ADD">
    <div class="right-flap"></div>
  </div>
</form>
</div>                        
</div>

    <div id="deleteProduct">
    <!-- Popup Div Starts Here -->
      <div id="popupDelete" class="popup">
          <!-- Contact Us Form -->
          <img id="close" src="assets/img/close.png" onclick="div_hide('deleteProduct')">
           <form  id="form" method="post" name="form" enctype="multipart/form-data"> 
              <hr>
              <h2>Are You Sure??</h2>
              <input type="submit" name="deleteProduct" value="OK">
              <input type="hidden" name="id" id="deleteId">
          </form>
      </div>
 </div>
<?php include "include/footer_script.php" ;?>
<script type="text/javascript">
  $(document).ready(function() {
     $('#ddlCars').multiselect();
      $('#ddlCars1').multiselect({ 
         numberDisplayed: 2,
     });
       
        $('#ddlCars3').multiselect({  
           nonSelectedText :'Select Branch'
           
     });
        $('#ddlCars2').multiselect({  
           nonSelectedText :'Select Product '
           
     });
});
</script>
</body>
</html>
