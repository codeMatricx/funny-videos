<?php

include "database.php";
if(isset($_POST["sub"]))
{
$target_dir = "assets/img/uploads/users/";
$image = $_FILES['image']["name"];
$target_file = $target_dir . basename($_FILES['image']["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

$name=$_POST["name"];
$email=$_POST["email"];
$mobile=$_POST["mobile"];
$DOB=$_POST['DOB'];
$gender=$_POST['gender'];
$cat=$_POST['cat'];
$status = 1;
$image="";
            if(empty($_FILES["image"]["tmp_name"]))
            {
            echo "Please choose File!";
            $uploadOk = 0;
            }
            else
            {
              if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
              && $imageFileType != "gif" ) 
              {
              echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
              $uploadOk = 0;
              }
              $check = getimagesize($_FILES["image"]["tmp_name"]);
              if($check != true) 
              {
              echo "File is not an image.";
              $uploadOk = 0;
              }
              else
              { 
                if ($_FILES["image"]["size"] > 500000)
                {
                echo "Sorry, your file is too large.";
                $uploadOk = 0;
                }
              }
            }
            if($uploadOk == 1)
            {
              if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file))
              {
              $image =  $_FILES["image"]["name"];
              echo "The file " . "&nbsp" . "<b>" . basename( $_FILES["image"]["name"]). "</b>" . "&nbsp" . " has been uploaded.";
              }
            }

function random_password( $length = 8 ) 
{
  $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
  $password = substr( str_shuffle( $chars ), 0, $length );
  return $password;
}
$get_rand_password = random_password();
$get_rand_password_sha = sha1($get_rand_password);
$sql = "INSERT INTO users (name, email, image,type,gender, date_of_birth,password,phone)
VALUES ('$name','$email','$image','$cat','$gender','$DOB','$get_rand_password_sha','$mobile')";
     if ($conn->query($sql) === TRUE) 
       {
          function clean_string($string)
          {
          $bad = array("content-type", "bcc:", "to:", "cc:", "href");
          return str_replace($bad, "", $string);
          }
          $email_to = "$email"; 
          $email_from = "customer@gmail.com"; // must be different than $email_from 
          $email_subject = "Password Recovery Mail";
          if(1)
          {
          $error_message = "";
          $email_message .= "Name: ".clean_string($name)."\n";
          $email_message .= "Password: ".clean_string($get_rand_password)."\n";
          $headers = 'From: '.$email_from."\r\n".
          'Reply-To: '.$email."\r\n" .
          'X-Mailer: PHP/' . phpversion();
          if (@mail($email_to, $email_subject, $email_message, $headers))
          {
          header("Location:user.php"); 
          }
          else 
          {
          echo json_encode(array('success'=>0, 'message'=>'An error occured. Please try again later.'));
          die();        
          }
          }
          echo "New record created successfully";
        } 
        else 
        {
        echo "Error: " . $sql . "<br>" . $conn->error;
        }
  }
  if (isset($_POST["deleteProduct"]))
  {
      $id = ($_POST['id']); 
      $sql = "DELETE FROM users WHERE id= $id";
      if ($conn->query($sql) === TRUE)
      {
          $responseMessage =  "User Remove successfully";
      }
      else
      {
          $responseMessage =  "Connection failed: " . $conn->error;
      }
  }
?>
<?php require('include/head.php'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 <?php include "include/header.php";?>
  <?php 
  include "include/left_sidebar.php"; ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Users Details  
      </h1>    
    </section>
   <section class="content">
     <div class="box">
            <div class="box-header">
              <h3 class="box-title">Users Table With Full Features</h3>
            </div>
            <div class="box-body table-responsive table-scroll-y">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>User ID</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Password</th>
                  <th>Mobile</th>
                  <th>Picture</th>
                  <th>Country</th>
                  <th>Created Time</th>
                  <th>Action</th>
                  <th><input name="select-all" id="checkall" onClick="check_uncheck_checkbox(this.checked);"  value="check_all"  type="checkbox"></th>
                </tr>
                </thead>
                <tbody>
                        <?php 
                        $sql = "SELECT * from users";
                        $result = $conn->query($sql);
                        if ($result->num_rows>0)
                        {
                        $serial=0;
                        while($user = $result->fetch_assoc())
                        {
                        $serial++;
                        ?>
                
                <tr id="<?php  echo $user['id'];?>">
                  <td><?php echo $serial; ?></td>
                  <td><?php echo $user['id']; ?></td>
                  <td><?php  echo $user['name'];?></td>
                  <td><?php  echo $user['email'];?></td>
                  <td><?php  echo $user['phone'];?></td>
                  <td><?php  echo $user['phone'];?></td>
                  <td><img src="assets/img/uploads/users/<?php echo $user['image']; ?>" class="img-responsive" style="width:30px; height:30px" ></td>
                  <td>India</td>
                  <td><?php  echo $user['created_date_time'];?></td>
                  <!-- <td><label class="switch">
                    <input type="checkbox" value="<?php echo $user['id'];?>" name="action" id="checkbox-1" onchange="togglechange(this,$(this).val())" checked>
                    <span class="slider round"></span>
                  </label>
                </td> -->
                  <td >
                       <a href="user_view.php?user_id=<?php  echo $user['id'];?>"  style="cursor: pointer;"><i class="fa fa-eye" aria-hidden="true"></i></a>
                      <!-- <a class="<?php echo $user['id'];?>" onclick="div_show('deleteProduct',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;"><i class="fa fa-trash-o" aria-hidden="true"></i></a> -->
                  </td>
                  <td><input type="checkbox" value="<?php echo $user['id'];?>" name="action" id="checkboxs"></td>
                </tr>
                <?php } } ?>
                </tbody>
              </table>
                <button class="btn btn-danger" onclick="deleteUser()">Delete</button>
            </div>
          </div>
    </section>
  </div>
  <?php include "include/footer.php" ;?>
  <?php include "include/right_sidebar.php" ;?>
</div>
 <div id="addNews">
  <div class="flappy-dialog">
  <h2>Add User</h2>
  <img id="close" src="assets/img/close.png" onclick="div_hide('addNews')" style="float: right; margin-top: -63px; padding-right: 6px;">
  <form  id="form" method="post" name="form" enctype="multipart/form-data"> 
  <input id="name" name="name" placeholder="User Name" type="text" required>
  <input id="email" name="email" placeholder="Email Id" type="email" required>
 <!--  <input id="password" name="password" placeholder="Password" type="password"> -->
  <input id="mobile" name="mobile" placeholder="Mobile No" type="number" maxlength=10 required>
  <input id="mobile" name="DOB" placeholder="Date of Birth" type="text" onfocus="(this.type='date')" onfocusout="(this.type='text')" required>
  <input type="radio" name="gender" value="1"  checked> Male
  <input type="radio" name="gender" value="2"  > Female
   <label for="newimage" class="btn text-muted text-center " style="width:82%;margin-top: 2%;">Choose Image</label>
   <input id="newimage" type="file" name="image" style="display:none">
  <select name="cat" class="form-control" id="category" required>
    <option disabled selected value="">Select Type</option>
    <option value="1">Seller</option>
    <option value="2">Admin</option>  
  </select>   
  <div class="flappy-dialog-buttons" style="margin-top: 5px;">
    <div class="left-flap"></div>
    <input type="submit" name="sub" value="ADD">
    <div class="right-flap"></div>
  </div>
</form>
</div>                       
</div>
    <div id="deleteProduct">
                    <!-- Popup Div Starts Here -->
                    <div id="popupDelete" class="popup">
                        <!-- Contact Us Form -->
                        <img id="close" src="assets/img/close.png" onclick="div_hide('deleteProduct')">
                       <form  id="form" method="post" name="form" enctype="multipart/form-data"> 
                            <hr>
                            <h2>Are You Sure??</h2>
                            <input type="submit" name="deleteProduct" value="OK">
                            <input type="hidden" name="id" id="deleteId">
                        </form>
                    </div>
 </div>                
<?php include "include/footer_script.php" ;?>
</body>
</html>
