<?php

include "database.php";

?>

<?php
if (isset($_POST["deleteProduct"]))
{
    $id = ($_POST['id']);
    // $sql = "SELECT image from women where id = $id";
    // $result = $conn->query($sql);
    // if ($result->num_rows>0)
    // {
    //     $data = $result->fetch_assoc();
    //     $deleteimage = $data['image'];
    // }
    $sql = "DELETE FROM users WHERE id= $id";
    if ($conn->query($sql) === TRUE)
    {
        //unlink($image_dir.$deleteimage);
        $responseMessage =  "User Remove successfully";
    }
    else
    {
        $responseMessage =  "Connection failed: " . $conn->error;
    }
}

?>
<?php require('include/head.php'); ?>


<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  
  <?php include "include/header.php";?>
  
  <!-- Left side column. contains the logo and sidebar -->
  <?php include "include/left_sidebar.php";?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Reservation Details
        
      </h1>
      
      
    </section>

    <!-- Main content -->

   <section class="content">
      <!-- Info boxes -->

     <div class="box">
            <div class="box-header">
              <h3 class="box-title">Reservation Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive table-scroll-y">
              <table id="example1" class="table table-bordered table-striped ">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Product Id</th>
                  <th>User Name</th>
                  <th>Date</th>
                  <th>Transaction No</th>
                  <th>Amount</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>

                   <?php 
                                    $sql = "SELECT * from reservation";
                                    $result = $conn->query($sql);
                                    if ($result->num_rows>0)
                                    {
                                        $serial=1;
                                        
                                        while($reservation = $result->fetch_assoc())
                                        {
                                            
                                    ?>
                <tr id="<?php  echo $reservation['id'];?>">
                  <td><?php echo $serial; ?></td>
                  <td><?php  echo $reservation['product_id'];?></td>
                  <td><?php  echo $reservation['name'];?></td>
                  <td><?php  echo $reservation['date'];?></td>
                  <td><?php  echo $reservation['transaction'];?></td>
                  <td><?php  echo $reservation['amount'];?> &#x24;</td>
                  <td>
                    <a href="reservation_view.php?uid=<?php echo $reservation['id'];?>"  style="cursor: pointer;"><i class="fa fa-eye" aria-hidden="true"></i></a>/
                    <a class="<?php echo $reservation['id'];?>" onclick="div_show('deleteProduct',$(this).attr('class'))" style="cursor: pointer;text-decoration: underline;"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                </tr>
                
                
                 <?php
                                        $serial++;
                                         } } ?>
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
      
      
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <?php include "include/footer.php" ;?>
  
  
  <!-- Control Sidebar -->
  <?php include "include/right_sidebar.php" ;?>
  
</div>
<!-- ./wrapper -->

 

    <div id="deleteProduct">
                    <!-- Popup Div Starts Here -->
                    <div id="popupDelete" class="popup">
                        <!-- Contact Us Form -->
                        <img id="close" src="assets/img/close.png" onclick="div_hide('deleteProduct')">
                        <form  id="form" method="post" name="form" enctype="multipart/form-data"> 
                            <hr>
                            <h2>Are You Sure??</h2>
                            <input type="submit" name="deleteProduct" value="OK">
                            <input type="hidden" name="id" id="deleteId">
                        </form>
                    </div>
 </div>                

<?php include "include/footer_script.php" ;?>




</body>
</html>
