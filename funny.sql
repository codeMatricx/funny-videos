-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 25, 2018 at 03:16 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `funny`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(255) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category_name`, `created_date_time`) VALUES
(25, 'Funny', '2018-01-10 08:13:13'),
(26, 'Good', '2018-01-10 08:13:21');

-- --------------------------------------------------------

--
-- Table structure for table `filter`
--

CREATE TABLE `filter` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `subCategoryId` int(11) NOT NULL,
  `priceFrom` int(11) NOT NULL,
  `priceTo` int(11) NOT NULL,
  `adsInKm` int(11) NOT NULL DEFAULT '30',
  `lati` float DEFAULT NULL,
  `lang` float DEFAULT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `filter`
--

INSERT INTO `filter` (`id`, `userId`, `categoryId`, `subCategoryId`, `priceFrom`, `priceTo`, `adsInKm`, `lati`, `lang`, `created_date_time`) VALUES
(5, 47, 5, 2, 200, 300, 10, NULL, NULL, '2018-01-04 16:47:06'),
(13, 47, 5, 0, 0, 0, 10, NULL, NULL, '2018-01-05 05:21:30'),
(7, 47, 5, 3, 200, 300, 10, NULL, NULL, '2018-01-04 17:05:01'),
(8, 47, 5, 4, 200, 300, 10, NULL, NULL, '2018-01-04 17:23:02'),
(12, 47, 5, 4, 210, 250, 30, NULL, NULL, '2018-01-05 05:20:42'),
(11, 70, 5, 4, 210, 250, 10, NULL, NULL, '2018-01-05 05:18:26'),
(17, 47, 0, 0, 0, 0, 0, NULL, NULL, '2018-01-05 05:52:00');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(255) NOT NULL,
  `gender_name` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `gender_name`, `created_date_time`) VALUES
(1, 'Male', '2017-12-19 10:43:35'),
(2, 'Female', '2017-12-19 10:43:50'),
(3, 'Others', '2017-12-19 10:44:08');

-- --------------------------------------------------------

--
-- Table structure for table `gifs`
--

CREATE TABLE `gifs` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `categories` varchar(255) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `view_count` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gifs`
--

INSERT INTO `gifs` (`id`, `type`, `categories`, `item_name`, `image`, `description`, `created_date_time`, `view_count`, `user_id`) VALUES
(56, '', '25', 'Funny Gif', 'gif2.gif', 'This is funny gifs', '2018-01-10 08:17:04', 3, 0),
(57, '', '26', 'Good Gif', 'gif3.gif', 'This is good gif', '2018-01-10 08:17:35', 1, 0),
(58, '2', '25', 'Funny Gifs3', 'ch4.jpg', 'Funny Gifs', '2018-01-23 13:53:21', 0, 0),
(59, '2', '25', 'Funny Gif10', 'gif6.gif', 'This is a funny video\r\n', '2018-01-25 09:24:47', 0, 0),
(60, '2', '26', 'This is funny gif', 'gif5.gif', 'This is funny gifs', '2018-01-25 09:29:56', 0, 0),
(61, '2', '25', 'Funny Gif', 'gif5.gif', 'This is gif', '2018-01-25 09:36:22', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `category_id` varchar(255) NOT NULL,
  `categories` varchar(255) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `video` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `category_id`, `categories`, `item_name`, `image`, `video`, `description`, `created_date_time`) VALUES
(25, '', '20', 'Funny Video', 'ch2.jpg', 'demo.mp4', 'This is funny', '2018-01-08 12:28:02'),
(26, '', '20', 'Funny Gif', 'ch4.jpg', 'demo.mp4', 'This is funny Gif', '2018-01-08 12:28:31'),
(29, '', '20', 'Funny Video', 'ch3.jpg', 'demo.mp4', 'Funny', '2018-01-08 15:37:58'),
(30, '', '19', 'Funny ', 'ch4.jpg', 'demo.mp4', 'Demo', '2018-01-08 18:58:07'),
(33, '', '19', 'Funny Video1', 'ch2.jpg', '01.Excel 2013 Tutorial 1 In HINDI - Basic Task (Part 1 ).mp4', 'Funny Video1', '2018-01-09 12:11:43'),
(34, '', '19', 'Funny Video2', 'ch3.jpg', '03.Excel 2013 Tutorial 3 In HINDI - How to insert and deletes rows and columns.mp4', 'Funny Video2', '2018-01-09 12:12:25'),
(37, '', '20', 'Funny Gif2', 'gif1.gif', '', 'This is funny Gif1', '2018-01-09 12:42:19'),
(39, '', '20', 'Funny Gif3', 'gif3.gif', '', 'This is Funny Gif3', '2018-01-09 12:44:04'),
(40, '', '20', 'Funny Gif4', 'gif4.gif', '', 'This is funny gifs4', '2018-01-09 12:46:43'),
(41, '', '20', 'This is funny gif5', 'gif5.gif', '', 'This is funny Gif5', '2018-01-09 12:47:16'),
(42, '', '20', 'Funny Gif6', 'gif6.gif', '', 'This is funny Gif6', '2018-01-09 12:47:50'),
(43, '', '20', 'Funny Gif7', 'gif7.gif', '', 'This is funny gif8', '2018-01-09 12:48:38'),
(44, '', '20', 'Funny Gif9', 'gif8.gif', '', 'Funny gif9', '2018-01-09 12:49:12'),
(45, '', '23', 'Funny Picture1', 'ch2.jpg', '', 'This is funny pictures', '2018-01-09 13:13:00'),
(46, '', '23', 'Funny Picture2', 'ch3.jpg', '', 'Funny Pictures', '2018-01-09 13:13:41'),
(47, '', '23', 'Funny Pictures3', 'ch4.jpg', '', 'Funny Pictures3', '2018-01-09 13:14:18');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `session_id` varchar(255) NOT NULL,
  `device_token` varchar(255) NOT NULL,
  `device_type` varchar(255) NOT NULL,
  `user_login_type` int(11) DEFAULT '0',
  `login_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `user_id`, `session_id`, `device_token`, `device_type`, `user_login_type`, `login_date_time`) VALUES
(80, 56, '657', '', '', 0, '2017-12-26 09:37:15'),
(81, 56, '791', '', '', 0, '2017-12-26 09:38:24'),
(83, 62, '537', '', '', 0, '2017-12-26 09:39:59'),
(85, 63, '491', '', '', 0, '2017-12-26 09:43:01'),
(103, 63, '833', '', '', 0, '2017-12-26 13:26:41'),
(104, 37, '998', '', '', 0, '2017-12-26 14:36:20'),
(105, 63, '577', '', '', 0, '2017-12-26 14:39:22'),
(128, 63, '495', '', '', 0, '2017-12-27 05:40:12'),
(129, 63, '813', '', '', 0, '2017-12-27 05:43:42'),
(131, 63, '929', '', '', 0, '2017-12-27 05:47:17'),
(136, 74, '790', '', '', 0, '2017-12-27 07:26:23'),
(179, 63, '837', '', '', 0, '2017-12-28 09:38:27'),
(180, 63, '956', '', '', 0, '2017-12-28 09:43:59'),
(181, 78, '845', '', '', 0, '2017-12-28 09:57:46'),
(208, 80, '996', '', '', 0, '2017-12-30 07:38:15'),
(209, 70, '641', '', '', 0, '2017-12-30 07:45:25'),
(211, 74, '581', '', '', 0, '2017-12-30 14:03:19'),
(212, 74, '895', '', '', 0, '2017-12-30 14:06:45'),
(213, 74, '445', '', '', 0, '2017-12-30 14:09:53'),
(214, 88, '658', '', '', 0, '2017-12-30 15:50:22'),
(215, 88, '820', '', '', 0, '2017-12-30 16:23:28'),
(221, 67, '694', '', '', 0, '2018-01-03 14:57:15'),
(222, 67, '643', '', '', 0, '2018-01-03 14:59:37'),
(223, 67, '631', '', '', 0, '2018-01-03 15:00:12'),
(224, 67, '927', '', '', 0, '2018-01-03 15:01:19'),
(225, 67, '868', '', '', 0, '2018-01-03 15:01:21'),
(226, 67, '913', '', '', 0, '2018-01-03 15:01:21'),
(227, 67, '745', '', '', 0, '2018-01-03 15:03:18'),
(228, 94, '902', '', '', 0, '2018-01-03 15:04:11'),
(229, 95, '678', '', '', 0, '2018-01-03 16:03:08'),
(230, 97, '710', '', '', 0, '2018-01-03 16:14:35'),
(231, 97, '806', '', '', 0, '2018-01-03 16:26:38'),
(232, 97, '869', '', '', 0, '2018-01-03 16:27:47'),
(237, 47, '686', '', '', 0, '2018-01-04 11:31:07'),
(239, 102, '993', '', '', 0, '2018-01-04 11:58:59'),
(241, 102, '783', '', '', 0, '2018-01-04 12:00:04'),
(243, 47, '982', '', '', 0, '2018-01-04 12:28:18'),
(244, 47, '556', '', '', 0, '2018-01-04 12:28:27'),
(245, 47, '560', '', '', 0, '2018-01-04 12:28:37'),
(246, 47, '721', '', '', 0, '2018-01-04 12:28:59'),
(247, 47, '633', '', '', 0, '2018-01-04 12:29:32'),
(248, 104, '627', '', '', 0, '2018-01-04 12:44:27'),
(249, 47, '481', '', '', 0, '2018-01-04 12:48:30'),
(250, 47, '851', '', '', 0, '2018-01-04 12:53:29'),
(251, 104, '934', '', '', 0, '2018-01-04 12:53:39'),
(252, 47, '956', '', '', 0, '2018-01-04 13:03:30'),
(253, 47, '975', '', '', 0, '2018-01-04 13:06:56'),
(254, 47, '654', '', '', 0, '2018-01-04 13:17:56'),
(255, 47, '579', '', '', 0, '2018-01-04 13:22:41'),
(256, 47, '620', '', '', 0, '2018-01-04 13:24:13'),
(257, 47, '456', '', '', 0, '2018-01-04 13:25:57'),
(258, 47, '790', '', '', 0, '2018-01-04 13:27:35'),
(259, 47, '467', '', '', 0, '2018-01-04 13:32:09'),
(260, 47, '811', '', '', 0, '2018-01-04 13:37:47'),
(261, 47, '466', '', '', 0, '2018-01-04 13:40:06'),
(262, 47, '860', '', '', 0, '2018-01-04 13:42:36'),
(263, 47, '617', '', '', 0, '2018-01-04 13:48:55'),
(264, 47, '673', '', '', 0, '2018-01-04 14:28:29'),
(265, 47, '458', '', '', 0, '2018-01-04 14:56:53'),
(266, 47, '775', '', '', 0, '2018-01-04 15:06:38'),
(267, 47, '639', '', '', 0, '2018-01-04 16:15:57');

-- --------------------------------------------------------

--
-- Table structure for table `pictures`
--

CREATE TABLE `pictures` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `categories` varchar(255) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `view_count` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pictures`
--

INSERT INTO `pictures` (`id`, `type`, `categories`, `item_name`, `image`, `description`, `created_date_time`, `view_count`, `user_id`) VALUES
(51, '', '25', 'Funny Picture', 'funny.jpg', 'This is funny picture', '2018-01-10 08:18:17', 1, 0),
(52, '', '26', 'Good Picture', 'ch3.jpg', 'This is good picture', '2018-01-10 08:18:44', 2, 0),
(53, '', '25', 'Funny pic2', 'g1.jpg', 'This is funny pic2', '2018-01-20 06:18:57', 1, 0),
(54, '3', '26', 'Funny Picture', 'ch4.jpg', 'Funny Picture', '2018-01-23 13:55:07', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `temporary_user`
--

CREATE TABLE `temporary_user` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `type` tinyint(4) NOT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `date_of_birth` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `social_id` varchar(255) DEFAULT NULL,
  `social_picture` varchar(255) DEFAULT NULL,
  `user_status` tinyint(4) NOT NULL,
  `is_driving_license` tinyint(1) NOT NULL DEFAULT '0',
  `is_credit_card_approved` tinyint(1) NOT NULL DEFAULT '0',
  `device_type` varchar(255) DEFAULT NULL,
  `device_token` varchar(255) DEFAULT NULL,
  `user_register_type` int(11) NOT NULL DEFAULT '3',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rating` varchar(100) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temporary_user`
--

INSERT INTO `temporary_user` (`id`, `name`, `email`, `password`, `image`, `type`, `gender`, `date_of_birth`, `phone`, `social_id`, `social_picture`, `user_status`, `is_driving_license`, `is_credit_card_approved`, `device_type`, `device_token`, `user_register_type`, `created_date_time`, `rating`) VALUES
(12, 'Admin', 'ravindra.aln@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', '5a4e0e0364b3b.jpeg', 3, NULL, NULL, '9898989898', NULL, NULL, 0, 0, 0, NULL, NULL, 3, '2018-01-04 11:20:35', '0'),
(13, 'Saumya', 'saumya@gmail.com', '7c222fb2927d828af22f592134e8932480637c0d', '5a4e0ffd4c648.jpeg', 3, NULL, NULL, '8077024283', NULL, NULL, 0, 0, 0, NULL, NULL, 3, '2018-01-04 11:29:01', '0'),
(14, 'Admin', 'ravindra.aln@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', '5a4e10e0aa9e1.jpeg', 3, NULL, NULL, '9898989898', NULL, NULL, 0, 0, 0, NULL, NULL, 3, '2018-01-04 11:32:48', '0'),
(16, 'Admin', 'ravindra.aln@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', '5a4e157ada1b1.jpeg', 3, NULL, NULL, '9898989898', NULL, NULL, 0, 0, 0, NULL, NULL, 3, '2018-01-04 11:52:27', '0');

-- --------------------------------------------------------

--
-- Table structure for table `temporary_user_document`
--

CREATE TABLE `temporary_user_document` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `creditcard` varchar(255) NOT NULL,
  `driving_licence` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temporary_user_document`
--

INSERT INTO `temporary_user_document` (`id`, `user_id`, `creditcard`, `driving_licence`, `address`, `description`, `created_date_time`) VALUES
(26, 14, '1234568763678', 'DL45HG241352', 'c-75,sector 10,Noida', 'Users Document Description', '2018-01-04 11:32:48'),
(25, 13, '123456789789465132', '123456789798465132', 'Noida', 'Hello', '2018-01-04 11:29:01'),
(24, 12, '1234568763678', 'DL45HG241352', 'c-75,sector 10,Noida', 'Users Document Description', '2018-01-04 11:20:35'),
(28, 16, '1234568763678', 'DL45HG241352', 'c-75,sector 10,Noida', 'Users Document Description', '2018-01-04 11:52:27');

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE `type` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`id`, `type`) VALUES
(1, 'Videos'),
(2, 'Gifs'),
(3, 'pictures');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `type` tinyint(4) NOT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `date_of_birth` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `social_id` varchar(255) DEFAULT NULL,
  `social_picture` varchar(255) DEFAULT NULL,
  `user_status` tinyint(4) NOT NULL,
  `is_driving_license` tinyint(1) NOT NULL DEFAULT '0',
  `is_credit_card_approved` tinyint(1) NOT NULL DEFAULT '0',
  `device_type` varchar(255) DEFAULT NULL,
  `device_token` varchar(255) DEFAULT NULL,
  `user_register_type` int(11) NOT NULL DEFAULT '3',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rating` varchar(100) NOT NULL DEFAULT '0',
  `country` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `image`, `type`, `gender`, `date_of_birth`, `phone`, `social_id`, `social_picture`, `user_status`, `is_driving_license`, `is_credit_card_approved`, `device_type`, `device_token`, `user_register_type`, `created_date_time`, `rating`, `country`) VALUES
(2, 'Funny Videos', 'funnyvideos@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', 'e1.jpg', 2, '1', '1979-09-19', '1234567899', NULL, NULL, 1, 1, 1, '', '', 3, '2017-12-19 11:59:16', '1', ''),
(47, 'Akash', 'ankita.dhaka09@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', '5a4e5db2552f5.jpeg', 3, '', '', '123456789', NULL, NULL, 1, 0, 0, '', '', 3, '2017-12-23 16:41:10', '2', ''),
(101, 'Akash', 'ravindra.aln@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', '5a4e21361b6ec.jpeg', 3, NULL, NULL, '123456788899', NULL, NULL, 0, 0, 0, NULL, NULL, 3, '2018-01-04 11:53:08', '0', ''),
(102, 'Akash', 'ravindra.4736@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', '5a4e1a977658e.jpeg', 3, NULL, NULL, '00007453545', NULL, NULL, 0, 0, 0, NULL, NULL, 3, '2018-01-04 11:58:01', '0', ''),
(103, 'Ankita Dhaka', 'ankita.dhaka091@gmail.com', '', NULL, 3, NULL, NULL, NULL, '1670581839668416', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/24991578_1641593455900588_772669502303455700_n.jpg?oh=deb1ca23ae741bdb74a55e53c8ccf548&oe=5AF175EF', 0, 0, 0, NULL, NULL, 1, '2018-01-04 12:26:32', '0', ''),
(106, '', 'ratan.pandey1994@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL, 3, NULL, NULL, '7042071224', NULL, NULL, 0, 0, 0, NULL, NULL, 3, '2018-01-09 05:54:41', '0', ''),
(107, '', 'admin', '38f078a81a2b033d197497af5b77f95b50bfcfb8', NULL, 3, NULL, NULL, '', NULL, NULL, 0, 0, 0, NULL, NULL, 3, '2018-01-09 07:29:55', '0', ''),
(108, '', 'ratan7139@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL, 3, NULL, NULL, '7042071224', NULL, NULL, 0, 0, 0, NULL, NULL, 3, '2018-01-09 11:22:44', '0', ''),
(109, '', 'ratan7139@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL, 3, NULL, NULL, '7042071224', NULL, NULL, 0, 0, 0, NULL, NULL, 3, '2018-01-09 11:23:32', '0', ''),
(110, '', 'ram@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL, 3, NULL, NULL, '7042071224', NULL, NULL, 0, 0, 0, NULL, NULL, 3, '2018-01-09 11:50:35', '0', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_document`
--

CREATE TABLE `user_document` (
  `id` int(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `creditcard` varchar(255) DEFAULT NULL,
  `driving_licence` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_document`
--

INSERT INTO `user_document` (`id`, `user_id`, `creditcard`, `driving_licence`, `address`, `description`, `created_date_time`) VALUES
(44, 47, 'TR4563783TY756', 'UP16TY3452', 'C-75 Sector-10 Noida ', 'I am A user', '2017-12-23 16:41:10'),
(89, 103, NULL, NULL, NULL, NULL, '2018-01-04 12:26:32'),
(90, 104, NULL, NULL, NULL, NULL, '2018-01-04 12:40:48'),
(88, 102, 'TR4563783TY756', 'UP16TY3452', 'C-75 Sector-10 Noida ', 'I am A user', '2018-01-04 11:58:01'),
(87, 101, 'TR4563783TY756', 'UP16TY3452', 'C-75 Sector-10 Noida ', 'I am A user', '2018-01-04 11:53:08'),
(86, 100, '123456789987654333', 'wetyyqtqreqeqeqeqeq334', 'noida sector 12', 'i am owner of our organization.', '2018-01-04 11:43:08');

-- --------------------------------------------------------

--
-- Table structure for table `user_register_type`
--

CREATE TABLE `user_register_type` (
  `id` int(11) NOT NULL,
  `register_type_name` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_register_type`
--

INSERT INTO `user_register_type` (`id`, `register_type_name`, `created_date_time`) VALUES
(1, 'Facebook', '2017-12-20 10:55:25'),
(2, 'Gmail', '2017-12-20 10:55:25'),
(3, 'App User', '2017-12-20 10:56:09');

-- --------------------------------------------------------

--
-- Table structure for table `user_verify`
--

CREATE TABLE `user_verify` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `otp` int(11) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_verify`
--

INSERT INTO `user_verify` (`id`, `email`, `otp`, `created_date_time`) VALUES
(26, 'saumya@gmail.com', 9635, '2018-01-04 11:29:01');

-- --------------------------------------------------------

--
-- Table structure for table `valid_for`
--

CREATE TABLE `valid_for` (
  `id` int(255) NOT NULL,
  `valid_for_name` varchar(100) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `valid_for`
--

INSERT INTO `valid_for` (`id`, `valid_for_name`, `created_date_time`) VALUES
(1, 'All Users', '2017-12-20 07:36:53'),
(2, 'Particular User', '2017-12-20 07:37:21');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `categories` varchar(255) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `video` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `view_count` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `type`, `categories`, `item_name`, `image`, `video`, `description`, `created_date_time`, `view_count`, `user_id`) VALUES
(50, '1', '25', 'Funny Video', 'ch2.jpg', '01.Excel 2013 Tutorial 1 In HINDI - Basic Task (Part 1 ).mp4', 'This Funny Video', '2018-01-10 08:15:12', 17, 0),
(51, '1', '26', 'Good Video', 'ch4.jpg', 'demo.mp4', 'This is good video', '2018-01-10 08:16:31', 1, 0),
(52, '1', '25', 'Funny Video3', 'g.jpg', '04.Excel 2013 Tutorial 4 in HINDI - Order of Operations _ Precedence (PEDMAS).mp4', 'This is funny video3', '2018-01-20 05:32:02', 2, 0),
(53, '1', '25', 'Funny Video2', 'g1.jpg', '01.Excel 2013 Tutorial 1 In HINDI - Basic Task (Part 1 ).mp4', 'Funny videos5', '2018-01-23 10:26:13', 0, 0),
(54, '1', '25', 'Funny Video', 'ch4.jpg', '06.Excel 2013 Tutorial 6 in HINDI - How to add password to Excel Worksheet -.mp4', 'Funny Video19', '2018-01-23 11:25:18', 0, 0),
(55, '1', '26', 'Funny Video3', 'ch2.jpg', '05.Excel 2013 Tutorial 5 in HINDI - Protecting Cells and Worksheets.mp4', 'Funny Video20', '2018-01-23 11:28:52', 0, 0),
(56, '1', '25', 'Funny Video', 'ch2.jpg', '06.Excel 2013 Tutorial 6 in HINDI - How to add password to Excel Worksheet -.mp4', 'Demo', '2018-01-23 11:36:38', 0, 0),
(57, '1', '25', 'Funny Video', 'ch3.jpg', 'demo.mp4', 'This is funny video', '2018-01-25 09:46:32', 0, 106);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `filter`
--
ALTER TABLE `filter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gifs`
--
ALTER TABLE `gifs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pictures`
--
ALTER TABLE `pictures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temporary_user`
--
ALTER TABLE `temporary_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temporary_user_document`
--
ALTER TABLE `temporary_user_document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_document`
--
ALTER TABLE `user_document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_register_type`
--
ALTER TABLE `user_register_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_verify`
--
ALTER TABLE `user_verify`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `valid_for`
--
ALTER TABLE `valid_for`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `filter`
--
ALTER TABLE `filter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `gifs`
--
ALTER TABLE `gifs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=268;

--
-- AUTO_INCREMENT for table `pictures`
--
ALTER TABLE `pictures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `temporary_user`
--
ALTER TABLE `temporary_user`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `temporary_user_document`
--
ALTER TABLE `temporary_user_document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `type`
--
ALTER TABLE `type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT for table `user_document`
--
ALTER TABLE `user_document`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `user_register_type`
--
ALTER TABLE `user_register_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_verify`
--
ALTER TABLE `user_verify`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `valid_for`
--
ALTER TABLE `valid_for`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
