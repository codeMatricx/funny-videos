<?php
class User {
	private $dbHost     = "localhost";
    private $dbUsername = "root";
    private $dbPassword = "";
    private $dbName     = "funny";
	private $userTbl    = 'users';
	
	function __construct(){
		if(!isset($this->db)){
            // Connect to the database
            $conn = new mysqli($this->dbHost, $this->dbUsername, $this->dbPassword, $this->dbName);
            if($conn->connect_error){
                die("Failed to connect with MySQL: " . $conn->connect_error);
            }else{
                $this->db = $conn;
            }
        }
	}
	
	function checkUser($userData = array()){
		if(!empty($userData)){
			// Check whether user data already exists in database
			$prevQuery = "SELECT * FROM ".$this->userTbl." WHERE email = '".$userData['email']."' ";
			$prevResult = $this->db->query($prevQuery);
			if($prevResult->num_rows > 0){
				// Update user data if already exists
				$query = "UPDATE ".$this->userTbl." SET name = '".$userData['name']."', email = '".$userData['email']."', gender = '".$userData['gender']."',  social_picture = '".$userData['picture']."',  created_date_time = '".date("Y-m-d H:i:s")."' WHERE email = '".$userData['email']."' ";
				$update = $this->db->query($query);
			}else{
				// Insert user data
				$query = "INSERT INTO ".$this->userTbl." SET email = '".$userData['email']."',  name = '".$userData['name']."', email = '".$userData['email']."', gender = '".$userData['gender']."', social_picture = '".$userData['picture']."',  created_date_time = '".date("Y-m-d H:i:s")."'";
				$insert = $this->db->query($query);
			}
			
			// Get user data from the database
			$result = $this->db->query($prevQuery);
			$userData = $result->fetch_assoc();
		}
		
		// Return user data
		return $userData;
	}
}
?>