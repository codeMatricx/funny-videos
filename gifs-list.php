<?php 
 include "database.php";
$gcid = $_GET['gif_cat_id'];
?>
<!--header-->
<?php include"include/header.php"; ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			<div class="main-grids">
				<div class="recommended">
					<div class="recommended-grids english-grid">
						<div class="recommended-info">
							<?php 
		                    $sql = "SELECT * FROM category WHERE id=$gcid ";
		                    $result = $conn->query($sql);
		                    if ($result->num_rows>0)
		                    {
		                    while($category = $result->fetch_assoc())
		                    {
		                    ?>
							<div class="heading">
								<h3>Top <?php echo $category['category_name'];?></h3>
							</div>
							<?php 
	                     	} 
	                    	}
	                     	?>
						   <div class="clearfix"> </div>
						</div>
						<?php 
	                    $sql = "SELECT * FROM gifs WHERE categories=$gcid ";
	                    $result = $conn->query($sql);
	                    if ($result->num_rows>0)
	                    {
	                    while($gif_cat = $result->fetch_assoc())
	                    {
	                    ?>
						<div class="col-md-3 resent-grid recommended-grid sports-recommended-grid">
							<div class="resent-grid-img recommended-grid-img">
								<a href="gifs.php?gif_id=<?php echo $gif_cat['id'];?>&gif_cat_id=<?php echo $gif_cat['categories'];?>"><img src="images/<?php echo $gif_cat['image'];?>" alt="" width="320" height="180" /></a>
							</div>
							<div class="resent-grid-info recommended-grid-info">
								<h5><a href="gifs.php?gifs_id=<?php echo $gif_cat['id'];?>" class="title"><?php echo $gif_cat['item_name'];?></a></h5>
								<p class="author"><a href="#" class="author">Admin</a></p>
								<p class="views"><?php echo $gif_cat['view_count'];?> views</p>
							</div>
						</div>
						<?php 
                     	} 
                    	}
                     	?>
						<div class="clearfix"> </div>
					</div>
				</div>
			</div>
<!-- footer -->
<?php include "include/footer.php"; ?>			