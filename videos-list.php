 <?php 
 include "database.php";
$vcid = $_GET['video_cat_id'];
?>
<!--header-->
<?php include"include/header.php"; ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			<div class="main-grids">
				<div class="recommended">
					<div class="recommended-grids english-grid">
						<div class="recommended-info">
						<?php 
		                    $sql = "SELECT * FROM category WHERE id=$vcid ";
		                    $result = $conn->query($sql);
		                    if ($result->num_rows>0)
		                    {
		                    while($category = $result->fetch_assoc())
		                    {
		                    ?>
							<div class="heading">
								<h3>Top <?php echo $category['category_name'];?></h3>
							</div>
							<?php 
	                     	} 
	                    	}
	                     	?>
							<div class="clearfix"> </div>
						</div>
						<?php 
	                    $sql = "SELECT * FROM videos WHERE categories=$vcid ";
	                    $result = $conn->query($sql);
	                    if ($result->num_rows>0)
	                    {
	                    while($video_cat = $result->fetch_assoc())
	                    {
	                    ?>
						<div class="col-md-3 resent-grid recommended-grid sports-recommended-grid">
							<div class="resent-grid-img recommended-grid-img">
								<a href="videos.php?video_id=<?php echo $video_cat['id'];?>&video_cat_id=<?php echo $video_cat['categories'];?>"><img src="images/<?php echo $video_cat['image'];?>" alt="" height="250px" /></a>
								<div class="time small-time sports-tome">
									<p>7:30</p>
								</div>
								<div class="clck sports-clock">
									<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
								</div>
							</div>
							<div class="resent-grid-info recommended-grid-info">
								<h5><a href="videos.php?video_id=<?php echo $video_cat['id'];?>&cat_id=<?php echo $video_cat['categories'];?>" class="title"><?php echo $video_cat['item_name'];?></a></h5>
								<p class="author"><a href="#" class="author">Admin</a></p>
								<p class="views"><?php echo $video_cat['view_count'];?> views</p>
							</div>
						</div>
						<?php 
                     	} 
                    	}
                     	?>
						<div class="clearfix"> </div>
					</div>
				</div>
			</div>
<!-- footer -->
<?php include "include/footer.php"; ?>			