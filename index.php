
<!--header-->
<?php include"include/header.php"; ?>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			<div class="main-grids">
				<div class="top-grids">
					<div class="recommended-info">
						<h3>Recent Videos</h3>
					</div>
					<?php 
                    $sql = "SELECT * FROM videos   LIMIT 6";
                    $result = $conn->query($sql);
                    if ($result->num_rows>0)
                    {
                    while($category_list = $result->fetch_assoc())
                    {
                    ?>
					<div class="col-md-4 resent-grid recommended-grid slider-top-grids">
						<div class="resent-grid-img recommended-grid-img">
							<a href="videos.php?video_id=<?php echo $category_list['id'];?>&video_cat_id=<?php echo $category_list['categories'];?>"><img src="images/<?php echo $category_list['image'];?>" alt="" height="250px" /></a>
							<!--<div class="time">
								<p>3:04</p>
							</div>
							<div class="clck">
								<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
							</div>-->
							<div class="clck">
								<span class="glyphicon glyphicon-play-circle" aria-hidden="true"></span>
							</div>
						</div>
						<div class="resent-grid-info recommended-grid-info">
							<h3><a href="videos.php" class="title title-info"><?php echo $category_list['item_name'];?></a></h3>
							<ul>
								<li>
									<ul>
									<p class="author author-info"><a href="#" class="author">Admin</a></p>
									</ul>
								</li>
								<li class="right-list"><p class="views views-info"><?php echo $category_list['view_count'];?> views</p></li>
							</ul>
						</div>
					</div>
					<?php 
                     	} 
                    	}
                     ?>
					<div class="clearfix"> </div>
				</div>
				<div class="recommended">
					<div class="recommended-grids">
						<div class="recommended-info">
							<h3>Gifs</h3>
						</div>
						<?php 
	                    $sql = "SELECT * FROM gifs  LIMIT 8";
	                    $result = $conn->query($sql);
	                    if ($result->num_rows>0)
	                    {
	                    while($category_gifs = $result->fetch_assoc())
	                    {
	                    ?>
						<div class="col-md-3 resent-grid recommended-grid">
							<div class="resent-grid-img recommended-grid-img">
								<a href="gifs.php?gif_id=<?php echo $category_gifs['id'];?>&gif_cat_id=<?php echo $category_gifs['categories'];?>"><img src="images/<?php echo $category_gifs['image'];?>" alt="" width="320" height="180" /></a>
								<!--<div class="time small-time">
									<p>2:34</p>
								</div>
								<div class="clck small-clck">
									<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
								</div>-->
							</div>
							<div class="resent-grid-info recommended-grid-info video-info-grid">
								<h5><a href="gifs.php" class="title"><?php echo $category_gifs['item_name'];?></a></h5>
								<ul>
									<li><p class="author author-info"><a href="#" class="author">Admin</a></p></li>
									<li class="right-list"><p class="views views-info"><?php echo $category_gifs['view_count'];?> views</p></li>
								</ul>
							</div>
						</div>
						<?php 
                     	} 
                    	}
                     	?>
						<div class="clearfix"> </div>
				</div>
			</div>
			<div class="recommended">
					<div class="recommended-grids">
						<div class="recommended-info">
							<h3>Pictures</h3>
						</div>
						<?php 
	                    $sql = "SELECT * FROM pictures  LIMIT 8";
	                    $result = $conn->query($sql);
	                    if ($result->num_rows>0)
	                    {
	                    while($category_pics = $result->fetch_assoc())
	                    {
	                    ?>
						<div class="col-md-3 resent-grid recommended-grid">
							<div class="resent-grid-img recommended-grid-img">
								<a href="pictures.php?pics_id=<?php echo $category_pics['id'];?>&pic_cat_id=<?php echo $category_pics['categories'];?>"><img src="images/<?php echo $category_pics['image'];?>" alt="" width="320" height="180" /></a>
								<!--<div class="time small-time">
									<p>2:34</p>
								</div>
								<div class="clck small-clck">
									<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
								</div>-->
							</div>
							<div class="resent-grid-info recommended-grid-info video-info-grid">
								<h5><a href="gifs.php" class="title"><?php echo $category_pics['item_name'];?></a></h5>
								<ul>
									<li><p class="author author-info"><a href="#" class="author">Admin</a></p></li>
									<li class="right-list"><p class="views views-info"><?php echo $category_pics['view_count'];?> views</p></li>
								</ul>
							</div>
						</div>
						<?php 
                     	} 
                    	}
                     	?>
						<div class="clearfix"> </div>
				</div>
			</div>
<!-- footer -->
<?php include "include/footer.php"; ?>			